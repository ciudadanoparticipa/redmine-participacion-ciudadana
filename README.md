# Redmine

# Crear la base de datos
## Ingresar al postgres y crear la db
- psql -U postgres
- CREATE DATABASE redmine OWNER postgres;
## Una vez creada la bd crear el esquema completo con el backups:
psql -U postgres -d redmine -1 -f backups/backups_20180918.sql

# Levantar servicio
## Para desarrollo
rails server
## Para producción
rails server -e production
