--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO redmine;

--
-- Name: attachments; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.attachments (
    id integer NOT NULL,
    container_id integer,
    container_type character varying(30),
    filename character varying DEFAULT ''::character varying NOT NULL,
    disk_filename character varying DEFAULT ''::character varying NOT NULL,
    filesize bigint DEFAULT 0 NOT NULL,
    content_type character varying DEFAULT ''::character varying,
    digest character varying(64) DEFAULT ''::character varying NOT NULL,
    downloads integer DEFAULT 0 NOT NULL,
    author_id integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    description character varying,
    disk_directory character varying
);


ALTER TABLE public.attachments OWNER TO redmine;

--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attachments_id_seq OWNER TO redmine;

--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.attachments_id_seq OWNED BY public.attachments.id;


--
-- Name: auth_sources; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.auth_sources (
    id integer NOT NULL,
    type character varying(30) DEFAULT ''::character varying NOT NULL,
    name character varying(60) DEFAULT ''::character varying NOT NULL,
    host character varying(60),
    port integer,
    account character varying,
    account_password character varying DEFAULT ''::character varying,
    base_dn character varying(255),
    attr_login character varying(30),
    attr_firstname character varying(30),
    attr_lastname character varying(30),
    attr_mail character varying(30),
    onthefly_register boolean DEFAULT false NOT NULL,
    tls boolean DEFAULT false NOT NULL,
    filter text,
    timeout integer
);


ALTER TABLE public.auth_sources OWNER TO redmine;

--
-- Name: auth_sources_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.auth_sources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_sources_id_seq OWNER TO redmine;

--
-- Name: auth_sources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.auth_sources_id_seq OWNED BY public.auth_sources.id;


--
-- Name: boards; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.boards (
    id integer NOT NULL,
    project_id integer NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying,
    "position" integer,
    topics_count integer DEFAULT 0 NOT NULL,
    messages_count integer DEFAULT 0 NOT NULL,
    last_message_id integer,
    parent_id integer
);


ALTER TABLE public.boards OWNER TO redmine;

--
-- Name: boards_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.boards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.boards_id_seq OWNER TO redmine;

--
-- Name: boards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.boards_id_seq OWNED BY public.boards.id;


--
-- Name: changes; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.changes (
    id integer NOT NULL,
    changeset_id integer NOT NULL,
    action character varying(1) DEFAULT ''::character varying NOT NULL,
    path text NOT NULL,
    from_path text,
    from_revision character varying,
    revision character varying,
    branch character varying
);


ALTER TABLE public.changes OWNER TO redmine;

--
-- Name: changes_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.changes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.changes_id_seq OWNER TO redmine;

--
-- Name: changes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.changes_id_seq OWNED BY public.changes.id;


--
-- Name: changeset_parents; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.changeset_parents (
    changeset_id integer NOT NULL,
    parent_id integer NOT NULL
);


ALTER TABLE public.changeset_parents OWNER TO redmine;

--
-- Name: changesets; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.changesets (
    id integer NOT NULL,
    repository_id integer NOT NULL,
    revision character varying NOT NULL,
    committer character varying,
    committed_on timestamp without time zone NOT NULL,
    comments text,
    commit_date date,
    scmid character varying,
    user_id integer
);


ALTER TABLE public.changesets OWNER TO redmine;

--
-- Name: changesets_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.changesets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.changesets_id_seq OWNER TO redmine;

--
-- Name: changesets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.changesets_id_seq OWNED BY public.changesets.id;


--
-- Name: changesets_issues; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.changesets_issues (
    changeset_id integer NOT NULL,
    issue_id integer NOT NULL
);


ALTER TABLE public.changesets_issues OWNER TO redmine;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.comments (
    id integer NOT NULL,
    commented_type character varying(30) DEFAULT ''::character varying NOT NULL,
    commented_id integer DEFAULT 0 NOT NULL,
    author_id integer DEFAULT 0 NOT NULL,
    content text,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE public.comments OWNER TO redmine;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO redmine;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.comments_id_seq OWNED BY public.comments.id;


--
-- Name: custom_field_enumerations; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.custom_field_enumerations (
    id integer NOT NULL,
    custom_field_id integer NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT true NOT NULL,
    "position" integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.custom_field_enumerations OWNER TO redmine;

--
-- Name: custom_field_enumerations_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.custom_field_enumerations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.custom_field_enumerations_id_seq OWNER TO redmine;

--
-- Name: custom_field_enumerations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.custom_field_enumerations_id_seq OWNED BY public.custom_field_enumerations.id;


--
-- Name: custom_fields; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.custom_fields (
    id integer NOT NULL,
    type character varying(30) DEFAULT ''::character varying NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    field_format character varying(30) DEFAULT ''::character varying NOT NULL,
    possible_values text,
    regexp character varying DEFAULT ''::character varying,
    min_length integer,
    max_length integer,
    is_required boolean DEFAULT false NOT NULL,
    is_for_all boolean DEFAULT false NOT NULL,
    is_filter boolean DEFAULT false NOT NULL,
    "position" integer,
    searchable boolean DEFAULT false,
    default_value text,
    editable boolean DEFAULT true,
    visible boolean DEFAULT true NOT NULL,
    multiple boolean DEFAULT false,
    format_store text,
    description text
);


ALTER TABLE public.custom_fields OWNER TO redmine;

--
-- Name: custom_fields_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.custom_fields_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.custom_fields_id_seq OWNER TO redmine;

--
-- Name: custom_fields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.custom_fields_id_seq OWNED BY public.custom_fields.id;


--
-- Name: custom_fields_projects; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.custom_fields_projects (
    custom_field_id integer DEFAULT 0 NOT NULL,
    project_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.custom_fields_projects OWNER TO redmine;

--
-- Name: custom_fields_roles; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.custom_fields_roles (
    custom_field_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.custom_fields_roles OWNER TO redmine;

--
-- Name: custom_fields_trackers; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.custom_fields_trackers (
    custom_field_id integer DEFAULT 0 NOT NULL,
    tracker_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.custom_fields_trackers OWNER TO redmine;

--
-- Name: custom_values; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.custom_values (
    id integer NOT NULL,
    customized_type character varying(30) DEFAULT ''::character varying NOT NULL,
    customized_id integer DEFAULT 0 NOT NULL,
    custom_field_id integer DEFAULT 0 NOT NULL,
    value text
);


ALTER TABLE public.custom_values OWNER TO redmine;

--
-- Name: custom_values_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.custom_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.custom_values_id_seq OWNER TO redmine;

--
-- Name: custom_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.custom_values_id_seq OWNED BY public.custom_values.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.documents (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    category_id integer DEFAULT 0 NOT NULL,
    title character varying DEFAULT ''::character varying NOT NULL,
    description text,
    created_on timestamp without time zone
);


ALTER TABLE public.documents OWNER TO redmine;

--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_id_seq OWNER TO redmine;

--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.documents_id_seq OWNED BY public.documents.id;


--
-- Name: email_addresses; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.email_addresses (
    id integer NOT NULL,
    user_id integer NOT NULL,
    address character varying NOT NULL,
    is_default boolean DEFAULT false NOT NULL,
    notify boolean DEFAULT true NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE public.email_addresses OWNER TO redmine;

--
-- Name: email_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.email_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.email_addresses_id_seq OWNER TO redmine;

--
-- Name: email_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.email_addresses_id_seq OWNED BY public.email_addresses.id;


--
-- Name: enabled_modules; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.enabled_modules (
    id integer NOT NULL,
    project_id integer,
    name character varying NOT NULL
);


ALTER TABLE public.enabled_modules OWNER TO redmine;

--
-- Name: enabled_modules_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.enabled_modules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enabled_modules_id_seq OWNER TO redmine;

--
-- Name: enabled_modules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.enabled_modules_id_seq OWNED BY public.enabled_modules.id;


--
-- Name: enumerations; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.enumerations (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    "position" integer,
    is_default boolean DEFAULT false NOT NULL,
    type character varying,
    active boolean DEFAULT true NOT NULL,
    project_id integer,
    parent_id integer,
    position_name character varying(30)
);


ALTER TABLE public.enumerations OWNER TO redmine;

--
-- Name: enumerations_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.enumerations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enumerations_id_seq OWNER TO redmine;

--
-- Name: enumerations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.enumerations_id_seq OWNED BY public.enumerations.id;


--
-- Name: groups_users; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.groups_users (
    group_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.groups_users OWNER TO redmine;

--
-- Name: import_items; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.import_items (
    id integer NOT NULL,
    import_id integer NOT NULL,
    "position" integer NOT NULL,
    obj_id integer,
    message text
);


ALTER TABLE public.import_items OWNER TO redmine;

--
-- Name: import_items_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.import_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.import_items_id_seq OWNER TO redmine;

--
-- Name: import_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.import_items_id_seq OWNED BY public.import_items.id;


--
-- Name: imports; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.imports (
    id integer NOT NULL,
    type character varying,
    user_id integer NOT NULL,
    filename character varying,
    settings text,
    total_items integer,
    finished boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.imports OWNER TO redmine;

--
-- Name: imports_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.imports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.imports_id_seq OWNER TO redmine;

--
-- Name: imports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.imports_id_seq OWNED BY public.imports.id;


--
-- Name: issue_categories; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.issue_categories (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    name character varying(60) DEFAULT ''::character varying NOT NULL,
    assigned_to_id integer
);


ALTER TABLE public.issue_categories OWNER TO redmine;

--
-- Name: issue_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.issue_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issue_categories_id_seq OWNER TO redmine;

--
-- Name: issue_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.issue_categories_id_seq OWNED BY public.issue_categories.id;


--
-- Name: issue_relations; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.issue_relations (
    id integer NOT NULL,
    issue_from_id integer NOT NULL,
    issue_to_id integer NOT NULL,
    relation_type character varying DEFAULT ''::character varying NOT NULL,
    delay integer
);


ALTER TABLE public.issue_relations OWNER TO redmine;

--
-- Name: issue_relations_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.issue_relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issue_relations_id_seq OWNER TO redmine;

--
-- Name: issue_relations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.issue_relations_id_seq OWNED BY public.issue_relations.id;


--
-- Name: issue_statuses; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.issue_statuses (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    is_closed boolean DEFAULT false NOT NULL,
    "position" integer,
    default_done_ratio integer
);


ALTER TABLE public.issue_statuses OWNER TO redmine;

--
-- Name: issue_statuses_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.issue_statuses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issue_statuses_id_seq OWNER TO redmine;

--
-- Name: issue_statuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.issue_statuses_id_seq OWNED BY public.issue_statuses.id;


--
-- Name: issues; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.issues (
    id integer NOT NULL,
    tracker_id integer NOT NULL,
    project_id integer NOT NULL,
    subject character varying DEFAULT ''::character varying NOT NULL,
    description text,
    due_date date,
    category_id integer,
    status_id integer NOT NULL,
    assigned_to_id integer,
    priority_id integer NOT NULL,
    fixed_version_id integer,
    author_id integer NOT NULL,
    lock_version integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    start_date date,
    done_ratio integer DEFAULT 0 NOT NULL,
    estimated_hours double precision,
    parent_id integer,
    root_id integer,
    lft integer,
    rgt integer,
    is_private boolean DEFAULT false NOT NULL,
    closed_on timestamp without time zone
);


ALTER TABLE public.issues OWNER TO redmine;

--
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_id_seq OWNER TO redmine;

--
-- Name: issues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.issues_id_seq OWNED BY public.issues.id;


--
-- Name: journal_details; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.journal_details (
    id integer NOT NULL,
    journal_id integer DEFAULT 0 NOT NULL,
    property character varying(30) DEFAULT ''::character varying NOT NULL,
    prop_key character varying(30) DEFAULT ''::character varying NOT NULL,
    old_value text,
    value text
);


ALTER TABLE public.journal_details OWNER TO redmine;

--
-- Name: journal_details_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.journal_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.journal_details_id_seq OWNER TO redmine;

--
-- Name: journal_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.journal_details_id_seq OWNED BY public.journal_details.id;


--
-- Name: journals; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.journals (
    id integer NOT NULL,
    journalized_id integer DEFAULT 0 NOT NULL,
    journalized_type character varying(30) DEFAULT ''::character varying NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    notes text,
    created_on timestamp without time zone NOT NULL,
    private_notes boolean DEFAULT false NOT NULL
);


ALTER TABLE public.journals OWNER TO redmine;

--
-- Name: journals_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.journals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.journals_id_seq OWNER TO redmine;

--
-- Name: journals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.journals_id_seq OWNED BY public.journals.id;


--
-- Name: member_roles; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.member_roles (
    id integer NOT NULL,
    member_id integer NOT NULL,
    role_id integer NOT NULL,
    inherited_from integer
);


ALTER TABLE public.member_roles OWNER TO redmine;

--
-- Name: member_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.member_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.member_roles_id_seq OWNER TO redmine;

--
-- Name: member_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.member_roles_id_seq OWNED BY public.member_roles.id;


--
-- Name: members; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.members (
    id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    mail_notification boolean DEFAULT false NOT NULL
);


ALTER TABLE public.members OWNER TO redmine;

--
-- Name: members_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.members_id_seq OWNER TO redmine;

--
-- Name: members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.members_id_seq OWNED BY public.members.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    board_id integer NOT NULL,
    parent_id integer,
    subject character varying DEFAULT ''::character varying NOT NULL,
    content text,
    author_id integer,
    replies_count integer DEFAULT 0 NOT NULL,
    last_reply_id integer,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL,
    locked boolean DEFAULT false,
    sticky integer DEFAULT 0
);


ALTER TABLE public.messages OWNER TO redmine;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO redmine;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: news; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.news (
    id integer NOT NULL,
    project_id integer,
    title character varying(60) DEFAULT ''::character varying NOT NULL,
    summary character varying(255) DEFAULT ''::character varying,
    description text,
    author_id integer DEFAULT 0 NOT NULL,
    created_on timestamp without time zone,
    comments_count integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.news OWNER TO redmine;

--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_id_seq OWNER TO redmine;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.news_id_seq OWNED BY public.news.id;


--
-- Name: open_id_authentication_associations; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.open_id_authentication_associations (
    id integer NOT NULL,
    issued integer,
    lifetime integer,
    handle character varying,
    assoc_type character varying,
    server_url bytea,
    secret bytea
);


ALTER TABLE public.open_id_authentication_associations OWNER TO redmine;

--
-- Name: open_id_authentication_associations_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.open_id_authentication_associations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.open_id_authentication_associations_id_seq OWNER TO redmine;

--
-- Name: open_id_authentication_associations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.open_id_authentication_associations_id_seq OWNED BY public.open_id_authentication_associations.id;


--
-- Name: open_id_authentication_nonces; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.open_id_authentication_nonces (
    id integer NOT NULL,
    "timestamp" integer NOT NULL,
    server_url character varying,
    salt character varying NOT NULL
);


ALTER TABLE public.open_id_authentication_nonces OWNER TO redmine;

--
-- Name: open_id_authentication_nonces_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.open_id_authentication_nonces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.open_id_authentication_nonces_id_seq OWNER TO redmine;

--
-- Name: open_id_authentication_nonces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.open_id_authentication_nonces_id_seq OWNED BY public.open_id_authentication_nonces.id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.projects (
    id integer NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description text,
    homepage character varying DEFAULT ''::character varying,
    is_public boolean DEFAULT true NOT NULL,
    parent_id integer,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    identifier character varying,
    status integer DEFAULT 1 NOT NULL,
    lft integer,
    rgt integer,
    inherit_members boolean DEFAULT false NOT NULL,
    default_version_id integer,
    default_assigned_to_id integer
);


ALTER TABLE public.projects OWNER TO redmine;

--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO redmine;

--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.projects_id_seq OWNED BY public.projects.id;


--
-- Name: projects_trackers; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.projects_trackers (
    project_id integer DEFAULT 0 NOT NULL,
    tracker_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.projects_trackers OWNER TO redmine;

--
-- Name: queries; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.queries (
    id integer NOT NULL,
    project_id integer,
    name character varying DEFAULT ''::character varying NOT NULL,
    filters text,
    user_id integer DEFAULT 0 NOT NULL,
    column_names text,
    sort_criteria text,
    group_by character varying,
    type character varying,
    visibility integer DEFAULT 0,
    options text
);


ALTER TABLE public.queries OWNER TO redmine;

--
-- Name: queries_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.queries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queries_id_seq OWNER TO redmine;

--
-- Name: queries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.queries_id_seq OWNED BY public.queries.id;


--
-- Name: queries_roles; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.queries_roles (
    query_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.queries_roles OWNER TO redmine;

--
-- Name: repositories; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.repositories (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    url character varying DEFAULT ''::character varying NOT NULL,
    login character varying(60) DEFAULT ''::character varying,
    password character varying DEFAULT ''::character varying,
    root_url character varying(255) DEFAULT ''::character varying,
    type character varying,
    path_encoding character varying(64) DEFAULT NULL::character varying,
    log_encoding character varying(64) DEFAULT NULL::character varying,
    extra_info text,
    identifier character varying,
    is_default boolean DEFAULT false,
    created_on timestamp without time zone
);


ALTER TABLE public.repositories OWNER TO redmine;

--
-- Name: repositories_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.repositories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.repositories_id_seq OWNER TO redmine;

--
-- Name: repositories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.repositories_id_seq OWNED BY public.repositories.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    "position" integer,
    assignable boolean DEFAULT true,
    builtin integer DEFAULT 0 NOT NULL,
    permissions text,
    issues_visibility character varying(30) DEFAULT 'default'::character varying NOT NULL,
    users_visibility character varying(30) DEFAULT 'all'::character varying NOT NULL,
    time_entries_visibility character varying(30) DEFAULT 'all'::character varying NOT NULL,
    all_roles_managed boolean DEFAULT true NOT NULL,
    settings text
);


ALTER TABLE public.roles OWNER TO redmine;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO redmine;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: roles_managed_roles; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.roles_managed_roles (
    role_id integer NOT NULL,
    managed_role_id integer NOT NULL
);


ALTER TABLE public.roles_managed_roles OWNER TO redmine;

--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO redmine;

--
-- Name: settings; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.settings (
    id integer NOT NULL,
    name character varying(255) DEFAULT ''::character varying NOT NULL,
    value text,
    updated_on timestamp without time zone
);


ALTER TABLE public.settings OWNER TO redmine;

--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_id_seq OWNER TO redmine;

--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.settings_id_seq OWNED BY public.settings.id;


--
-- Name: time_entries; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.time_entries (
    id integer NOT NULL,
    project_id integer NOT NULL,
    user_id integer NOT NULL,
    issue_id integer,
    hours double precision NOT NULL,
    comments character varying(1024),
    activity_id integer NOT NULL,
    spent_on date NOT NULL,
    tyear integer NOT NULL,
    tmonth integer NOT NULL,
    tweek integer NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone NOT NULL
);


ALTER TABLE public.time_entries OWNER TO redmine;

--
-- Name: time_entries_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.time_entries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.time_entries_id_seq OWNER TO redmine;

--
-- Name: time_entries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.time_entries_id_seq OWNED BY public.time_entries.id;


--
-- Name: tokens; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.tokens (
    id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    action character varying(30) DEFAULT ''::character varying NOT NULL,
    value character varying(40) DEFAULT ''::character varying NOT NULL,
    created_on timestamp without time zone NOT NULL,
    updated_on timestamp without time zone
);


ALTER TABLE public.tokens OWNER TO redmine;

--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tokens_id_seq OWNER TO redmine;

--
-- Name: tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.tokens_id_seq OWNED BY public.tokens.id;


--
-- Name: trackers; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.trackers (
    id integer NOT NULL,
    name character varying(30) DEFAULT ''::character varying NOT NULL,
    is_in_chlog boolean DEFAULT false NOT NULL,
    "position" integer,
    is_in_roadmap boolean DEFAULT true NOT NULL,
    fields_bits integer DEFAULT 0,
    default_status_id integer
);


ALTER TABLE public.trackers OWNER TO redmine;

--
-- Name: trackers_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.trackers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trackers_id_seq OWNER TO redmine;

--
-- Name: trackers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.trackers_id_seq OWNED BY public.trackers.id;


--
-- Name: user_preferences; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.user_preferences (
    id integer NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    others text,
    hide_mail boolean DEFAULT true,
    time_zone character varying
);


ALTER TABLE public.user_preferences OWNER TO redmine;

--
-- Name: user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_preferences_id_seq OWNER TO redmine;

--
-- Name: user_preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.user_preferences_id_seq OWNED BY public.user_preferences.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.users (
    id integer NOT NULL,
    login character varying DEFAULT ''::character varying NOT NULL,
    hashed_password character varying(40) DEFAULT ''::character varying NOT NULL,
    firstname character varying(30) DEFAULT ''::character varying NOT NULL,
    lastname character varying(255) DEFAULT ''::character varying NOT NULL,
    admin boolean DEFAULT false NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    last_login_on timestamp without time zone,
    language character varying(5) DEFAULT ''::character varying,
    auth_source_id integer,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    type character varying,
    identity_url character varying,
    mail_notification character varying DEFAULT ''::character varying NOT NULL,
    salt character varying(64),
    must_change_passwd boolean DEFAULT false NOT NULL,
    passwd_changed_on timestamp without time zone
);


ALTER TABLE public.users OWNER TO redmine;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO redmine;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: versions; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.versions (
    id integer NOT NULL,
    project_id integer DEFAULT 0 NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying DEFAULT ''::character varying,
    effective_date date,
    created_on timestamp without time zone,
    updated_on timestamp without time zone,
    wiki_page_title character varying,
    status character varying DEFAULT 'open'::character varying,
    sharing character varying DEFAULT 'none'::character varying NOT NULL
);


ALTER TABLE public.versions OWNER TO redmine;

--
-- Name: versions_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.versions_id_seq OWNER TO redmine;

--
-- Name: versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.versions_id_seq OWNED BY public.versions.id;


--
-- Name: watchers; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.watchers (
    id integer NOT NULL,
    watchable_type character varying DEFAULT ''::character varying NOT NULL,
    watchable_id integer DEFAULT 0 NOT NULL,
    user_id integer
);


ALTER TABLE public.watchers OWNER TO redmine;

--
-- Name: watchers_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.watchers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.watchers_id_seq OWNER TO redmine;

--
-- Name: watchers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.watchers_id_seq OWNED BY public.watchers.id;


--
-- Name: wiki_content_versions; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.wiki_content_versions (
    id integer NOT NULL,
    wiki_content_id integer NOT NULL,
    page_id integer NOT NULL,
    author_id integer,
    data bytea,
    compression character varying(6) DEFAULT ''::character varying,
    comments character varying(1024) DEFAULT ''::character varying,
    updated_on timestamp without time zone NOT NULL,
    version integer NOT NULL
);


ALTER TABLE public.wiki_content_versions OWNER TO redmine;

--
-- Name: wiki_content_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.wiki_content_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_content_versions_id_seq OWNER TO redmine;

--
-- Name: wiki_content_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.wiki_content_versions_id_seq OWNED BY public.wiki_content_versions.id;


--
-- Name: wiki_contents; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.wiki_contents (
    id integer NOT NULL,
    page_id integer NOT NULL,
    author_id integer,
    text text,
    comments character varying(1024) DEFAULT ''::character varying,
    updated_on timestamp without time zone NOT NULL,
    version integer NOT NULL
);


ALTER TABLE public.wiki_contents OWNER TO redmine;

--
-- Name: wiki_contents_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.wiki_contents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_contents_id_seq OWNER TO redmine;

--
-- Name: wiki_contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.wiki_contents_id_seq OWNED BY public.wiki_contents.id;


--
-- Name: wiki_pages; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.wiki_pages (
    id integer NOT NULL,
    wiki_id integer NOT NULL,
    title character varying(255) NOT NULL,
    created_on timestamp without time zone NOT NULL,
    protected boolean DEFAULT false NOT NULL,
    parent_id integer
);


ALTER TABLE public.wiki_pages OWNER TO redmine;

--
-- Name: wiki_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.wiki_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_pages_id_seq OWNER TO redmine;

--
-- Name: wiki_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.wiki_pages_id_seq OWNED BY public.wiki_pages.id;


--
-- Name: wiki_redirects; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.wiki_redirects (
    id integer NOT NULL,
    wiki_id integer NOT NULL,
    title character varying,
    redirects_to character varying,
    created_on timestamp without time zone NOT NULL,
    redirects_to_wiki_id integer NOT NULL
);


ALTER TABLE public.wiki_redirects OWNER TO redmine;

--
-- Name: wiki_redirects_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.wiki_redirects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_redirects_id_seq OWNER TO redmine;

--
-- Name: wiki_redirects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.wiki_redirects_id_seq OWNED BY public.wiki_redirects.id;


--
-- Name: wikis; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.wikis (
    id integer NOT NULL,
    project_id integer NOT NULL,
    start_page character varying(255) NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.wikis OWNER TO redmine;

--
-- Name: wikis_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.wikis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wikis_id_seq OWNER TO redmine;

--
-- Name: wikis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.wikis_id_seq OWNED BY public.wikis.id;


--
-- Name: workflows; Type: TABLE; Schema: public; Owner: redmine
--

CREATE TABLE public.workflows (
    id integer NOT NULL,
    tracker_id integer DEFAULT 0 NOT NULL,
    old_status_id integer DEFAULT 0 NOT NULL,
    new_status_id integer DEFAULT 0 NOT NULL,
    role_id integer DEFAULT 0 NOT NULL,
    assignee boolean DEFAULT false NOT NULL,
    author boolean DEFAULT false NOT NULL,
    type character varying(30),
    field_name character varying(30),
    rule character varying(30)
);


ALTER TABLE public.workflows OWNER TO redmine;

--
-- Name: workflows_id_seq; Type: SEQUENCE; Schema: public; Owner: redmine
--

CREATE SEQUENCE public.workflows_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workflows_id_seq OWNER TO redmine;

--
-- Name: workflows_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: redmine
--

ALTER SEQUENCE public.workflows_id_seq OWNED BY public.workflows.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.attachments ALTER COLUMN id SET DEFAULT nextval('public.attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.auth_sources ALTER COLUMN id SET DEFAULT nextval('public.auth_sources_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.boards ALTER COLUMN id SET DEFAULT nextval('public.boards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.changes ALTER COLUMN id SET DEFAULT nextval('public.changes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.changesets ALTER COLUMN id SET DEFAULT nextval('public.changesets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.comments ALTER COLUMN id SET DEFAULT nextval('public.comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.custom_field_enumerations ALTER COLUMN id SET DEFAULT nextval('public.custom_field_enumerations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.custom_fields ALTER COLUMN id SET DEFAULT nextval('public.custom_fields_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.custom_values ALTER COLUMN id SET DEFAULT nextval('public.custom_values_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.documents ALTER COLUMN id SET DEFAULT nextval('public.documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.email_addresses ALTER COLUMN id SET DEFAULT nextval('public.email_addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.enabled_modules ALTER COLUMN id SET DEFAULT nextval('public.enabled_modules_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.enumerations ALTER COLUMN id SET DEFAULT nextval('public.enumerations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.import_items ALTER COLUMN id SET DEFAULT nextval('public.import_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.imports ALTER COLUMN id SET DEFAULT nextval('public.imports_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issue_categories ALTER COLUMN id SET DEFAULT nextval('public.issue_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issue_relations ALTER COLUMN id SET DEFAULT nextval('public.issue_relations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issue_statuses ALTER COLUMN id SET DEFAULT nextval('public.issue_statuses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issues ALTER COLUMN id SET DEFAULT nextval('public.issues_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.journal_details ALTER COLUMN id SET DEFAULT nextval('public.journal_details_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.journals ALTER COLUMN id SET DEFAULT nextval('public.journals_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.member_roles ALTER COLUMN id SET DEFAULT nextval('public.member_roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.members ALTER COLUMN id SET DEFAULT nextval('public.members_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.news ALTER COLUMN id SET DEFAULT nextval('public.news_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.open_id_authentication_associations ALTER COLUMN id SET DEFAULT nextval('public.open_id_authentication_associations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.open_id_authentication_nonces ALTER COLUMN id SET DEFAULT nextval('public.open_id_authentication_nonces_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.projects ALTER COLUMN id SET DEFAULT nextval('public.projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.queries ALTER COLUMN id SET DEFAULT nextval('public.queries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.repositories ALTER COLUMN id SET DEFAULT nextval('public.repositories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.settings ALTER COLUMN id SET DEFAULT nextval('public.settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.time_entries ALTER COLUMN id SET DEFAULT nextval('public.time_entries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.tokens ALTER COLUMN id SET DEFAULT nextval('public.tokens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.trackers ALTER COLUMN id SET DEFAULT nextval('public.trackers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.user_preferences ALTER COLUMN id SET DEFAULT nextval('public.user_preferences_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.versions ALTER COLUMN id SET DEFAULT nextval('public.versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.watchers ALTER COLUMN id SET DEFAULT nextval('public.watchers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_content_versions ALTER COLUMN id SET DEFAULT nextval('public.wiki_content_versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_contents ALTER COLUMN id SET DEFAULT nextval('public.wiki_contents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_pages ALTER COLUMN id SET DEFAULT nextval('public.wiki_pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_redirects ALTER COLUMN id SET DEFAULT nextval('public.wiki_redirects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wikis ALTER COLUMN id SET DEFAULT nextval('public.wikis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.workflows ALTER COLUMN id SET DEFAULT nextval('public.workflows_id_seq'::regclass);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
environment	development	2018-08-31 14:10:06.274799	2018-08-31 14:10:06.274799
\.


--
-- Data for Name: attachments; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.attachments (id, container_id, container_type, filename, disk_filename, filesize, content_type, digest, downloads, author_id, created_on, description, disk_directory) FROM stdin;
\.


--
-- Name: attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.attachments_id_seq', 1, false);


--
-- Data for Name: auth_sources; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.auth_sources (id, type, name, host, port, account, account_password, base_dn, attr_login, attr_firstname, attr_lastname, attr_mail, onthefly_register, tls, filter, timeout) FROM stdin;
\.


--
-- Name: auth_sources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.auth_sources_id_seq', 1, false);


--
-- Data for Name: boards; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.boards (id, project_id, name, description, "position", topics_count, messages_count, last_message_id, parent_id) FROM stdin;
\.


--
-- Name: boards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.boards_id_seq', 1, false);


--
-- Data for Name: changes; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.changes (id, changeset_id, action, path, from_path, from_revision, revision, branch) FROM stdin;
\.


--
-- Name: changes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.changes_id_seq', 1, false);


--
-- Data for Name: changeset_parents; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.changeset_parents (changeset_id, parent_id) FROM stdin;
\.


--
-- Data for Name: changesets; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.changesets (id, repository_id, revision, committer, committed_on, comments, commit_date, scmid, user_id) FROM stdin;
\.


--
-- Name: changesets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.changesets_id_seq', 1, false);


--
-- Data for Name: changesets_issues; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.changesets_issues (changeset_id, issue_id) FROM stdin;
\.


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.comments (id, commented_type, commented_id, author_id, content, created_on, updated_on) FROM stdin;
\.


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.comments_id_seq', 1, false);


--
-- Data for Name: custom_field_enumerations; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.custom_field_enumerations (id, custom_field_id, name, active, "position") FROM stdin;
\.


--
-- Name: custom_field_enumerations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.custom_field_enumerations_id_seq', 1, false);


--
-- Data for Name: custom_fields; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.custom_fields (id, type, name, field_format, possible_values, regexp, min_length, max_length, is_required, is_for_all, is_filter, "position", searchable, default_value, editable, visible, multiple, format_store, description) FROM stdin;
3	ProjectCustomField	projectInfo	text	\N		\N	\N	f	f	f	1	f	{}	t	t	f	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\ntext_formatting: ''\n	Contain information about project in json format for use in asuparticipa frontend
4	IssueCustomField	Nombre del Reclamante	string	\N		\N	\N	f	t	t	3	t		t	t	f	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\ntext_formatting: ''\nurl_pattern: ''\n	Name's citizen that created the issue
5	IssueCustomField	Correo del reclamante	string	\N		\N	\N	f	t	t	4	t		t	t	f	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\ntext_formatting: full\n	Email's citizen that created the issue
2	IssueCustomField	Longitud	float	\N		-180	180	f	t	t	2	f	0	t	t	f	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nurl_pattern: ''\n	Longitude where find of the issue
1	IssueCustomField	Latitud	float	\N		-90	90	f	t	t	1	f	0	t	t	f	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\nurl_pattern: ''\n	Latitude where find of the issue
6	IssueCustomField	Identificador del reclamo	string	\N		\N	\N	f	t	f	5	t		t	t	f	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\ntext_formatting: ''\nurl_pattern: ''\n	Identificador utilizado para mostrarle al ciudadano que hizo el reclamo la url para hacer seguimiento
\.


--
-- Name: custom_fields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.custom_fields_id_seq', 6, true);


--
-- Data for Name: custom_fields_projects; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.custom_fields_projects (custom_field_id, project_id) FROM stdin;
\.


--
-- Data for Name: custom_fields_roles; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.custom_fields_roles (custom_field_id, role_id) FROM stdin;
\.


--
-- Data for Name: custom_fields_trackers; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.custom_fields_trackers (custom_field_id, tracker_id) FROM stdin;
1	1
1	2
1	3
2	1
2	2
2	3
4	1
4	2
4	3
5	1
5	2
5	3
6	1
6	2
6	3
\.


--
-- Data for Name: custom_values; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.custom_values (id, customized_type, customized_id, custom_field_id, value) FROM stdin;
92	Issue	24	4	Edgar Valdez
93	Issue	24	5	arturovolpe@gmail.com
102	Issue	27	1	-25.283232
103	Issue	27	2	-57.627422
9	Project	1	3	{\r\n    "id": 1,\r\n    "code": "costanera_sur",\r\n    "name": "Avenida Costanera Sur",\r\n    "types": [\r\n      "Vial - Movilidad",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2013,\r\n      "bidding": 2018,\r\n      "execution": 2019,\r\n      "finished": 2022\r\n    },\r\n    "slogan": "Construyendo el anillo de circunvalación de Asunción",\r\n    "families": {\r\n      "amount": 1000,\r\n      "message": "",\r\n      "popover": "Alrededor de 1000 familias residentes en el área"\r\n    },\r\n    "number": 1,\r\n    "costo": 200000000,\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg"\r\n    ],\r\n    "documentos": [\r\n      { "name": "CostaneraSur PBC adenda 6", "link": "CostaneraSur_PBC_adenda_6.pdf"},\r\n      { "name": "Costanera Sur PBCAddenda Nº 6. Llamado MOPC 123-2017", "link": "Costanera Sur_PBCAddenda Nº 6. Llamado MOPC 123-2017.pdf"}\r\n    ],\r\n    "descripcion": "Proyecto vial de 8 km de longitud, parte de la Franja Costera Sur, inicia el primer anillo de circunvalación de Asunción y crea un nuevo distrito urbano en el bañado sur. Facilitará el acceso ordenado y fluido al microcentro de la ciudad desde los alrededores del Cerro Lambaré hasta el Cerro Tacumbú. Este proyecto de infraestructura a ser construido en tierras de la Municipalidad de Asunción será ejecutado por el Ministerio de Obras Públicas y Comunicaciones, como resultado de un pacto político, social, ambiental, económico y financiero acordado entre los moradores del bañado sur y sus organizaciones de base, la Municipalidad de Asunción, el Gobierno Nacional, SENAVITAT e Itaipú Binacional, entre otros actores.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Sur",\r\n      "superficie": 80,\r\n      "fuenteFinanciacion": "Recursos del tesoro",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "MOPC" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#f47920",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "27.2%",\r\n      "left": "77.77%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.292269,\r\n        -57.659416\r\n      ],\r\n      "parent": true,\r\n      "zoom": 13,\r\n      "center": [\r\n        -25.316519,\r\n        -57.64861\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "right": "10%",\r\n        "width": "9.5rem"\r\n      },\r\n      "arrowStyles": {\r\n        "right": "0.8125rem"\r\n      }\r\n    }\r\n  }
10	Project	2	3	{\r\n    "id": 2,\r\n    "code": "banco_san_miguel",\r\n    "name": "Banco San Miguel",\r\n    "types": [\r\n      "Recuperación Ambiental"\r\n    ],\r\n    "status": {\r\n      "planification": 2018,\r\n      "bidding": 2019,\r\n      "execution": 2020,\r\n      "finished": 2025\r\n    },\r\n    "slogan": "Preservando el medioambiente frente al microcentro",\r\n    "families": {\r\n      "amount": 200,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 2,\r\n    "costo": 12000000,\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg",\r\n      "03.jpg",\r\n      "04.jpg",\r\n      "05.jpg",\r\n      "06.jpg",\r\n      "07.jpg"\r\n    ],\r\n    "documentos": [\r\n      "Banco San Miguel_Hacia El Plan De Manejo Del Banco San Miguel_publicación MCA.pdf",\r\n      "Banco San Miguel_plan de manejo_contextos grales.cap 1-21.pdf",\r\n      "Banco San Miguel_Ley N°2715 que declara area silvestre protegida.pdf",\r\n      "Banco San Miguel_Preparan el contenido de la propuesta del Plan de Manejo del Banco San Miguel_ publicación seam.pdf"\r\n    ],\r\n    "descripcion": "Proyecto de parquización de la reserva con el propósito de proteger y mantener la calidad medioambiental del banco, promover la convivencia social de las comunidades en espacios recreativos, fomentando el sustento productivo de los pescadores y el turismo sustentable en la zona. La infraestructura a ser construida en el área está localizada en la bahía de Asunción - frente mismo al microcentro capitalino. Tendrá un diseño que evite o minimice el impacto ambiental y la presión antrópica sobre el parque.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "300",\r\n      "fuenteFinanciacion": "A definir",\r\n      "costoEstimativo": "",\r\n      "beneficiarios": "",\r\n      "enteEjecutor": [\r\n        "Municipalidad de Asunción"\r\n      ]\r\n\r\n    },\r\n    "color": "#072d01",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "53.71%",\r\n      "left": "17.23%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.25232,\r\n        -57.61309\r\n      ],\r\n      "parent": true,\r\n      "zoom": 14,\r\n      "center": [\r\n        -25.264551,\r\n        -57.610085\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "left": "-85%"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "calc(50% - 7px)"\r\n      }\r\n    }\r\n  }
11	Project	3	3	{\r\n    "id": 3,\r\n    "code": "parque_arroyo_antequera",\r\n    "name": "Parque Arroyo Antequera",\r\n    "types": [\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2017,\r\n      "bidding": 2019,\r\n      "execution": 2019,\r\n      "finished": 2020\r\n    },\r\n    "slogan": "Devolviendo al agua su uso público",\r\n    "families": {\r\n      "amount": 0,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 3,\r\n    "costo": 500000,\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg",\r\n      "03.jpg",\r\n      "04.jpg",\r\n      "05.jpg",\r\n      "06.jpg",\r\n      "07.jpg",\r\n      "08.jpg",\r\n      "09.jpg",\r\n      "10.jpg",\r\n      "11.jpg"\r\n    ],\r\n    "descripcion": "Proyecto de parquización que se adapta a la forma natural del arroyo Antequera, prácticamente dentro del cauce, como continuidad del plan de mejoramiento del Barrio Chacharita Alta. El proyecto dotará al área de un equipamiento de uso público, dando valor al relieve natural, la riqueza de la fauna y la flora y del sistema hídrico.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "2",\r\n      "fuenteFinanciacion": "Recursos Genuinos Municipalidad de Asunción",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "Municipalidad de Asunción" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#1fc605",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "38.6%",\r\n      "left": "42.83%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.283232,\r\n        -57.627422\r\n      ],\r\n      "parent": true,\r\n      "zoom": 18,\r\n      "center": [\r\n        -25.283932,\r\n        -57.627323\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "right",\r\n      "styles": {\r\n        "left": "110%",\r\n        "top": "-22%"\r\n      },\r\n      "arrowStyles": {}\r\n    }\r\n  }
12	Project	4	3	{\r\n    "id": 4,\r\n    "code": "parque_caballero",\r\n    "name": "Parque Caballero",\r\n    "types": [\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2011,\r\n      "bidding": 2017,\r\n      "execution": 2018,\r\n      "finished": 2025\r\n    },\r\n    "slogan": "Reconciliando a la historia con el río",\r\n    "families": {\r\n      "amount": 300,\r\n      "message": "Alrededor de 240 familias residían y 60 residen en el área",\r\n      "popover": ""\r\n    },\r\n    "number": 4,\r\n    "costo": 5000000,\r\n    "images": [\r\n      "01.jpg"\r\n    ],\r\n    "documentos": [\r\n      { "name": "Gobierno Nacional proyecta revitalizar el Histórico  Parque Caballero_publicación_adndigital", "link": "http://www.adndigital.com.py/gobierno-proyecta-recobrar-el-historico-parque-caballero/" },\r\n      "Parque Caballero_Recuperación y ampliación del famoso Parque Caballero_publicación Itaipu 1.pdf",\r\n      "Parque Caballero_En marcha, ampliación y recuperación del Parque Caballero_publicación Itaipu 2.pdf",\r\n      "Parque Caballero_Recuperación y ampliación del famoso Parque Caballero_publicación Itaipu 3.pdf",\r\n      { "name": "Ya está vigente el acuerdo para la construcción de la Franja Costera Sur y la Revitalización del Parque Caballero_publicación MCA", "link": "http://www.asuncion.gov.py/intendencia/ya-esta-vigente-acuerdo-la-construccion-la-franja-costera-sur-la-revitalizacion-del-parque-caballero" },\r\n      { "name": "Parque Caballero estaría renovado en 2 años_publicación", "link": "https://www.ultimahora.com/parque-caballero-estaria-renovado-2-anos-n1150276.html" },\r\n      "Parque Caballero_Resolución JM_4941-2017 que aprueba Acuerdo Cooperación JEC.JE_158_2017_MCA - MOPC.pdf"\r\n    ],\r\n    "descripcion": "Proyecto de restauración y revitalización del Parque Caballero, uno de los espacios públicos más emblemáticos de Asunción, con alto valor patrimonial e histórico. Se buscará reposicionar al Parque como centro de encuentros sociales y culturales como elemento de unión entre el Río Paraguay y la ciudad de Asunción.",\r\n    "componentes": [\r\n      {\r\n        "name": "Parque Alto",\r\n        "images": [\r\n          "01.jpg"\r\n        ],\r\n        "descripcion": "Espacio principalmente dedicado al desarrollo de una reserva natural y espacios de recreación y esparcimiento. \\n<strong>Reserva natural</strong> | Vivero municipal. \\n<strong>Espacio público</strong> | Terraza mirador. \\n<strong>Emprendimientos privados</strong> | Zona de Foodparks. \\n<strong>Equipamiento urbano</strong> | Iluminación pública - Mobiliario urbano. \\n<strong>Área:</strong> 12,8 ha."\r\n      },\r\n      {\r\n        "name": "Parque Bajo",\r\n        "images": [\r\n          "01.jpg"\r\n        ],\r\n        "descripcion": "Espacio dedicado a emprendimientos culturales y espacios públicos al aire libre, junto con un conjunto de viviendas sociales. \\n<strong>Equipamiento urbano</strong> | Mobiliario urbano - Iluminación pública. \\n<strong>Espacios públicos</strong> | Conexiones: Escalinatas, rampas, camineros. - Anfiteatro <strong>Viviendas sociales</strong> | Barrio refugio. \\n<strong>Defensa contra inundaciones</strong> | Relleno cota +64 \\n<strong>Área:</strong> 9, 2 ha."\r\n      }\r\n    ],\r\n    "fichaTecnica": {\r\n      "ubicacion": "Franja Costera Norte Avenida Artigas (Parque Alto) y Avenida Costanera José Asunción Flores",\r\n      "superficie": "Parque alto: 13 Has. - Parque bajo: 9 Has",\r\n      "fuenteFinanciacion": "ITAIPU Binacional",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "Itaipú", "Municipalidad de Asunción" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#137903",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "35.51%",\r\n      "left": "33.1%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.279236,\r\n        -57.618076\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.280796,\r\n        -57.61584\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "top",\r\n      "styles": {\r\n        "left": "-75%",\r\n        "width": "7.125rem"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "calc(50% - 7px)"\r\n      }\r\n    }\r\n  }
13	Project	5	3	{\r\n    "id": 5,\r\n    "code": "parque_lineal_costero",\r\n    "name": "Parque Lineal Costero Norte",\r\n    "types": [\r\n      "Ordenamiento urbano",\r\n      "Vial - Movilidad",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2010,\r\n      "bidding": 2017,\r\n      "execution": 2018,\r\n      "finished": 2019\r\n    },\r\n    "slogan": "Paseando entre el río y la ciudad",\r\n    "families": {\r\n      "amount": 0,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 5,\r\n    "costo": 1200000,\r\n    "images": [\r\n      "01.png",\r\n      "02.jpg",\r\n      "03.jpg",\r\n      "04.jpg",\r\n      "05.jpg",\r\n      "06.jpg",\r\n      "07.jpg",\r\n      "08.jpg",\r\n      "09.jpg",\r\n      "10.jpg",\r\n      "11.jpg",\r\n      "12.jpg",\r\n      "13.jpg",\r\n      "14.jpg",\r\n      "15.png",\r\n      "16.jpg",\r\n      "17.jpg",\r\n      "18.jpg",\r\n      "19.jpg",\r\n      "20.jpg",\r\n      "21.jpg"\r\n    ],\r\n    "descripcion": "Proyecto de parquización de 12 hectáreas, en diseño lineal, de 40 metros de ancho, que sigue el trazado de la Avda. Costanera en toda su longitud, conformando el paseo costero norte hasta el Jardín Botánico. Contendrá una exuberante vegetación con reforestación de la zona rellenada, amplios espacios abiertos, zonas de juegos para niños, circuitos para bicicletas, caminatas, cami-trotes y explanadas para eventos, que complementan a otros espacios públicos preexistentes tales como: el Parque del Bicentenario, el Parque Caballero, el Muelle Deportivo, áreas gastronómicas, puerto revitalizado, entre otros.",\r\n    "componentes": [\r\n      {\r\n        "name": "Primera Etapa",\r\n        "images": [\r\n          "01.png",\r\n          "02.jpg",\r\n          "03.jpg",\r\n          "04.jpg",\r\n          "05.jpg",\r\n          "06.jpg",\r\n          "07.jpg",\r\n          "08.jpg",\r\n          "09.jpg",\r\n          "10.jpg",\r\n          "11.jpg",\r\n          "12.jpg",\r\n          "13.jpg",\r\n          "14.jpg",\r\n          "15.png"\r\n        ],\r\n        "descripcion": "Acompaña el trazado de la Primera Etapa de la Avenida Costanera y el relleno que concluyó en el año 2016, y se prevé la ejecución de equipamiento urbano adecuado y un plan arborización que potenciará las áreas verdes en espacios públicos de calidad."\r\n      },\r\n      {\r\n        "name": "Segunda Etapa",\r\n        "images": [\r\n          "16.jpg",\r\n          "17.jpg",\r\n          "18.jpg",\r\n          "19.jpg",\r\n          "20.jpg",\r\n          "21.jpg"\r\n        ],\r\n        "descripcion": "En esta fase se ejecutará el relleno integral, además de complementar el equipamiento urbano y  plan de arborización; conforme al convenio ya firmado entre el MOPC y la Municipalidad de Asunción, y continuar con el pacto social de dotar a la ciudad de espacios verdes y públicos de calidad."\r\n      }\r\n    ],\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "1era Etapa: 3 km / 2da Etapa: 5 km",\r\n      "fuenteFinanciacion": "1era Etapa: Recursos del Tesoro / 2da Etapa: A definirse",\r\n      "costoEstimativo": "1.200.000 US$ (ambas etapas)",\r\n      "enteEjecutor": [ "MOPC" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "documentos": [\r\n      {"name": "Parque Lineal PBC", "link": "https://drive.google.com/file/d/1wH-dESZp6qsZDunScx3Mu0RGoTOnZLO2/view"}\r\n    ],\r\n    "color": "#0d5302",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "34.99%",\r\n      "left": "18.21%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.248075,\r\n        -57.603947\r\n      ],\r\n      "parent": true,\r\n      "zoom": 14,\r\n      "center": [\r\n        -25.259796,\r\n        -57.592583\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "left": "-59%",\r\n        "width": "6.25rem"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "calc(50% - 8px)"\r\n      }\r\n    }\r\n  }
104	Issue	27	4	Arturo Volpe
105	Issue	27	5	edgarvaldez99@gmail.com
118	Issue	31	1	-25.306519
119	Issue	31	2	-57.65432
120	Issue	31	4	Arturo Volpe
121	Issue	31	5	edgarvaldez99@gmail.com
134	Issue	35	1	-25.306519
135	Issue	35	2	-57.65432
136	Issue	35	4	Arturo Volpe
137	Issue	35	5	arturovolpe@gmail.com
138	Issue	36	1	-25.306519
139	Issue	36	2	-57.65432
14	Project	6	3	{\r\n    "id": 6,\r\n    "code": "mirador_puerto",\r\n    "name": "Plaza Mirador del Puerto de Asunción",\r\n    "types": [\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2012,\r\n      "bidding": 2019,\r\n      "execution": 2019,\r\n      "finished": 2020\r\n    },\r\n    "slogan": "Contemplando el horizonte de Asunción",\r\n    "families": {\r\n      "amount": 0,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 6,\r\n    "costo": 500000,\r\n    "description": "",\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg",\r\n      "03.jpg",\r\n      "04.jpg",\r\n      "05.jpg",\r\n      "06.jpg",\r\n      "07.jpg",\r\n      "08.jpg",\r\n      "09.jpg",\r\n      "10.png"\r\n    ],\r\n    "documentos": [\r\n    ],\r\n    "descripcion": "Proyecto de plaza integrada al plan de Reconversión Urbana del Puerto de Asunción. Se ubicará en medio de edificios históricos y emblemáticos para revitalizar el espacio público preexistente, dándole realce al monumento artístico que ya se encuentra en el lugar. Se pretende ordenar la zona gastronómica, fortaleciendo los empleos, incluso posibilitando que los vendedores de comida de calle, ya instalados de manera desordenada en la zona, continúen trabajando en un ambiente más atractivo y ordenado.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Puerto de Asunción",\r\n      "superficie": "4200 m2",\r\n      "fuenteFinanciacion": "Recursos Genuinos Municipalidad de Asunción",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "Municipalidad de Asunción" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#19a004",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "53%",\r\n      "left": "50.29%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.275934,\r\n        -57.641132\r\n      ],\r\n      "parent": true,\r\n      "zoom": 18,\r\n      "center": [\r\n        -25.275934,\r\n        -57.640934\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "top",\r\n      "styles": {\r\n        "left": "14%",\r\n        "width": "8.4375rem"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.8125rem"\r\n      }\r\n    }\r\n  }
15	Project	7	3	{\r\n    "id": 7,\r\n    "code": "puerto_deportivo",\r\n    "name": "Puerto Deportivo de la Bahía",\r\n    "types": [\r\n      "Infraestructura",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2012,\r\n      "bidding": 2015,\r\n      "execution": 2016,\r\n      "finished": 2019\r\n    },\r\n    "slogan": "Navegando aventuras deportivas",\r\n    "families": {\r\n      "amount": 0,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 7,\r\n    "costo": 3000000,\r\n    "description": "",\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg",\r\n      "03.png"\r\n    ],\r\n    "documentos": [\r\n      {"name": "Información general del Proyecto Puerto Deportivo de la Bahía en  páginaTembiapó Paraguay", "link": "https://tembiapo.mopc.gov.py/obras/284-construccion-del-muelle-deportivo-de-la-bahia-de-asuncion"},\r\n      {"name": "Puerto Deportivo de la Bahía PBC", "link": "https://drive.google.com/open?id=1G3WBer8VIRddMrmEOA9WSLI2N51kIjkR"}\r\n    ],\r\n\r\n    "descripcion": "Proyecto de construcción de muelle, plataformas y otras obras de infraestructura, para equipar la Costanera de Asunción con un programa de uso deportivo-náutico, que contará con accesos directos a la zona de la bahía y con espacios cubiertos para depósitos y guarderías de embarcaciones. Este proyecto se hará en asociación con varias organizaciones que ya realizan actividades deportivas náuticas.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte - Bahía de Asunción",\r\n      "superficie": "2.2",\r\n      "fuenteFinanciacion": "Recursos del Tesoro",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "MOPC" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#03a9f4",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "41.63%",\r\n      "left": "31.99%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.274712,\r\n        -57.62785\r\n      ],\r\n      "parent": true,\r\n      "zoom": 17,\r\n      "center": [\r\n        -25.275612,\r\n        -57.62675\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "left",\r\n      "styles": {\r\n        "top": "-0.625rem",\r\n        "right": "100%",\r\n        "width": "7.5rem"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.8125rem"\r\n      }\r\n    }\r\n  }
16	Project	8	3	{\r\n    "id": 8,\r\n    "code": "puerto_asuncion",\r\n    "name": "Reconversión Urbana del Puerto de Asunción y Terminal del Metrobús",\r\n    "types": [\r\n      "Espacio Público",\r\n      "Oficinas de Gobierno"\r\n    ],\r\n    "status": {\r\n      "planification": 2010,\r\n      "bidding": 2016,\r\n      "execution": 2017,\r\n      "finished": 2025\r\n    },\r\n    "slogan": "Transformando el puerto en ciudad",\r\n    "families": {\r\n      "amount": 0,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n\r\n    "documentos": [\r\n      {"name": "Reconversión Urbana Puerto y Metrobus Ley 5133", "link": "https://drive.google.com/open?id=1-z6XpzjhfhZuPV5EkVKem3wJMkXd2i2q"},\r\n      {"name": "Reconversión Urbana Puerto y Metrobus Propuesta de préstamo BID", "link": "https://drive.google.com/open?id=1BHm_hovfEuPTif-Ay5DeXt0lX2tvMuBK"},\r\n      {"name": "Reconversión Urbana Puerto y Metrobus_Convenio Marco addenda 1", "link": "https://drive.google.com/open?id=16FQhisR7KFkINpDt7L2I8jiK41EoP0Os"}\r\n    ],\r\n    "number": 8,\r\n    "costo": 117000000,\r\n    "componentes": [\r\n      {\r\n        "name": "Reconversión Urbana del Puerto de Asunción",\r\n        "images": [\r\n          "01.jpeg",\r\n          "02.png",\r\n          "03.png",\r\n          "04.png",\r\n          "05.png",\r\n          "06.png",\r\n          "07.png",\r\n          "08.png",\r\n          "09.png",\r\n          "10.png"\r\n        ],\r\n        "descripcion": "<p>Este subproyecto de revitalización de terrenos y galpones de la zona portuaria para convertirla en un espacio recreativo y gastronómico, con polo comercial, áreas específicas para edificios corporativos privados y nuevos predios destinados a varios Ministerios del Gobierno Nacional, que conformarán un nuevo Centro Cívico de fácil acceso para toda la ciudadanía. </p><strong>Espacios Públicos</strong><p>Se incorporará el Puerto de Asunción al sistema de espacios públicos de la ciudad, actualmente explotada para espacios de expresión artística y cultural, el cual será potenciado e impulsado mediante el equipamiento urbano de grandes áreas internas y costeras del predio.</p><strong>Oficinas de Gobierno</strong><p>Se realizará la construcción de un nuevo conjunto de edificios que albergarán a Ministerios del Poder Ejecutivo, con la finalidad de crear una nueva centralidad de gestión y trabajo público dentro de un ambiente urbano de alta calidad, y de fácil acceso para la ciudadanía.</p>"\r\n      },\r\n      {\r\n        "name": "Terminal Metrobús",\r\n        "slogan": "Conectando a la ciudad con los ciudadanos",\r\n        "images": [\r\n          "11.jpg"\r\n        ],\r\n        "descripcion": "La Terminal del Metrobús ubicada en el Puerto de Asunción complementa el proyecto Metrobús, que dotará a la ciudad de un sistema de transporte público renovado y de calidad, para facilitar el acceso de toda la ciudadanía a las centralidades más importantes de Asunción y Gran Asunción. Esta Terminal, al igual que las demás, contará con plataformas de acceso a servicios troncales, alimentadores, y además, espacios públicos para el tránsito de personas que impulsará el comercio de trabajadores ambulantes en cada terminal, principalmente en la zona que se prevé como la de mayor tránsito, el Puerto de Asunción.",\r\n        "fichaTecnica": {\r\n          "ubicacion": "Puerto de Asunción",\r\n          "superficie": "5200 m2",\r\n          "fuenteFinanciacion": "Crédito BID",\r\n          "costoEstimativo": "A definir",\r\n          "enteEjecutor": [ "MOPC" ],\r\n          "beneficiarios": "",\r\n          "tipo": [\r\n            "Vial - Movilidad",\r\n            "Espacio Público"\r\n          ]\r\n        }\r\n      }\r\n    ],\r\n    "fichaTecnica": {\r\n      "ubicacion": "Franja Costera Norte - Avenida Stella Marys",\r\n      "superficie": "27",\r\n      "fuenteFinanciacion": "MOPC - BID - Municipalidad de Asunción",\r\n      "costoEstimativo": "117.000.000 US$ (Inversión en Infraestructura y Edificios Públicos)",\r\n      "enteEjecutor": [ "MOPC" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#0277bd",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "60%",\r\n      "left": "56.6%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.274742,\r\n        -57.641983\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.276625,\r\n        -57.63884\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "right",\r\n      "styles": {\r\n        "top": "-35%",\r\n        "left": "110%",\r\n        "width": "10.125rem"\r\n      },\r\n      "arrowStyles": { }\r\n    }\r\n  }
17	Project	9	3	{\r\n    "id": 9,\r\n    "code": "villa_olimpica",\r\n    "name": "Villa Olímpica",\r\n    "types": [\r\n      "Urbanización privada",\r\n      "Vial - Movilidad",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2018,\r\n      "bidding": 2019,\r\n      "execution": 2019,\r\n      "finished": 2030\r\n    },\r\n    "slogan": "Compitiendo para ganar",\r\n    "families": {\r\n      "amount": 0,\r\n      "message": "A definir de acuerdo con la selección definitiva de ubicación de la Villa.",\r\n      "popover": ""\r\n    },\r\n    "number": 9,\r\n    "costo": 0,\r\n    "images": [\r\n      "01.png",\r\n      "02.png",\r\n      "03.png",\r\n      "04.png",\r\n      "05.png",\r\n      "06.png"\r\n    ],\r\n    "documentos": [\r\n      {"name": "Visto bueno de la Comisión de Evaluación del ODESUR publicación snd", "link": "http://www.snd.gov.py/index.php/noticias/visto-bueno-de-la-comision-de-evaluacion-del-oedesur"},\r\n      {"name": "Villa Olímpica - Ambicioso proyecto con candidatura a los Juegos ODESUR 2022 publicación diario hoy.com.py", "link": "https://drive.google.com/open?id=1OvfnvdxOWpbhw2i7BF9ABJLdI8536VRO"},\r\n      {"name": "Villa Olímpica Odesur 2022 incluye construcción de villa olímpica publicación Agencia IP", "link": "https://drive.google.com/open?id=1GDZw-ERTfygoS38OJGVs-G9AJe-jbZ1v"},\r\n      {"name": "Villa Olímpica Presentación proyecto", "link": "https://drive.google.com/open?id=1i3zsrKKcEkGQhDRd1ZeMiWFiuPqOkf7c"},\r\n      {"name": "Villa Olímpica_publicación.paraguay.com", "link": "https://drive.google.com/open?id=1dNcnkFAv72VzPx7ut_doV4fCEohtWreh"}\r\n    ],\r\n    "descripcion": "Proyecto de creación de la Villa de Alojamiento para las delegaciones de atletas participantes de los Juegos Suramericanos 2022(Organización Deportiva Suramericana - ODESUR) en la ciudad de Asunción, elegida como sede para los juegos olímpicos. La Villa posteriormente será destinada a la comercialización en el mercado inmobiliario del segmento medio.",\r\n    "componentes": [\r\n      {\r\n        "name": "Primera Etapa",\r\n        "images": [\r\n          "01.png",\r\n          "02.png",\r\n          "03.png",\r\n          "04.png",\r\n          "05.png",\r\n          "06.png"\r\n        ],\r\n        "descripcion": "La primera etapa consiste en la construcción integral de la Villa Olímpica que albergará a las delegaciones de distintos países que participarán de los juegos olímpicos."\r\n      },\r\n      {\r\n        "name": "Segunda Etapa",\r\n        "images": [\r\n          "01.png",\r\n          "02.png",\r\n          "03.png",\r\n          "04.png",\r\n          "05.png",\r\n          "06.png"\r\n        ],\r\n        "descripcion": "A partir de la culminación de los juegos olímpicos, la zona será destinada a desarrolladores inmobiliarios para impulsar la repoblación de Asunción por el segmento medio de la población, actualmente expulsado a las afueras de la ciudad por los altos precios inmobiliarios."\r\n      }\r\n    ],\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "10",\r\n      "fuenteFinanciacion": "A definir",\r\n      "costoEstimativo": "A definir",\r\n      "enteEjecutor": [ "Municipalidad de Asunción", "Gobierno Nacional" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#0d2563",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "33.291%",\r\n      "left": "14.6%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.265102,\r\n        -57.605503\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.268103,\r\n        -57.605503\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "top",\r\n      "styles": {\r\n        "left": "15%"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.8125rem"\r\n      }\r\n    }\r\n  }
18	Project	10	3	{\r\n    "id": 10,\r\n    "code": "centro_eventos",\r\n    "name": "Eco-Bahía Centro de Eventos",\r\n    "types": [\r\n      "Urbanización privada",\r\n      "Vial - Ordenamiento urbano",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2018,\r\n      "bidding": 2019,\r\n      "execution": 2022,\r\n      "finished": 2029\r\n    },\r\n    "slogan": "Impulsando un distrito espectacular",\r\n    "families": {\r\n      "amount": 100,\r\n      "message": "Aprox. 100 familias residentes en el área",\r\n      "popover": ""\r\n    },\r\n    "number": 10,\r\n    "costo": 357000000,\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg"\r\n    ],\r\n    "descripcion": "Proyecto de desarrollo de un nuevo distrito en la ciudad de Asunción, en la Costanera Norte, con espacios públicos de calidad, desarrollos inmobiliarios mixtos, equipamientos recreativos, estructura para espectáculos culturales y deportivos. Incluye la recuperación del histórico arroyo Cará-Cará, para convertirlo en un canal abierto para la práctica del remo. El proyecto utilizará por primera vez la innovadora ordenanza municipal 136/2000 de canje de obras por tierras.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "25",\r\n      "fuenteFinanciacion": "APP",\r\n      "costoEstimativo": "47.000.000 US$ (en Infraestructura basica y equipamiento urbano) 310.000.000 US$ (Capital Privado. Superficie estimada de construcción a desarrollarse por el sector privado: 600.000 m2)",\r\n      "enteEjecutor": [ "APP (Asociación Público Privada)" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#ffc300",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "33.2%",\r\n      "left": "0%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.251669,\r\n        -57.606754\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.254969,\r\n        -57.604755\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "left": "15%"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.8125rem"\r\n      }\r\n    }\r\n  }
19	Project	11	3	{\r\n    "id": 11,\r\n    "code": "costanera_norte",\r\n    "name": "Avenida Costanera Norte de Asunción - Segunda Etapa y Conexión Avda. Primer Presidente con Ruta Nacional Nº 9",\r\n    "types": [\r\n      "Vial - Movilidad",\r\n      "Vial - Ordenamiento urbano",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2015,\r\n      "bidding": 2015,\r\n      "execution": 2016,\r\n      "finished": 2019\r\n    },\r\n    "slogan": "Transitando entre verdes desde el Palacio de López hasta el Jardín Botánico",\r\n    "families": {\r\n      "amount": 450,\r\n      "message": "Alrededor de 450 familias residían en el área",\r\n      "popover": ""\r\n    },\r\n    "documentos": [\r\n      {"name": "Avenida Costanera Norte Segunda Etapa PBC", "link": "https://drive.google.com/open?id=1vtKZCaC6wX2amwXsDbjsjcN4GucSvJUb"},\r\n      {"name": "Avenida Costanera Norte Segunda Etapa Convenio JBZA", "link": "https://drive.google.com/open?id=1VQTqoK7rBZGf6oKDHpZJjxwtubgdFRzG"},\r\n      {"name": "Plan de Reasentamiento Octubre-2017", "link": "https://www.mopc.gov.py/proyecto-de-mejoramiento-rehabilitacion-ampliacion-y-pavimentacion-asfaltica-de-la-ruta-n-9-transchaco-p39"},\r\n      {"name": "Información general del Proyecto Avenida Costanera Norte de Asunción - 2da. Etapa y Conexión Avda. Primer Presidente con la Ruta Nacional Nº 9, en páginaTembiapó Paraguay", "link": "https://tembiapo.mopc.gov.py/obras/90-construccion-de-la-avda-costanera-norte-de-asuncion-2da-etapa-banado-norte-y-conexion-a-la-avda-1er-presidente-con-la-ruta-n-9"}\r\n    ],\r\n    "number": 11,\r\n    "costo": 108500000,\r\n    "componentes": [\r\n      {\r\n        "name": "Avenida Costanera Norte Segunda Etapa",\r\n        "images": [\r\n          "01.jpg",\r\n          "02.jpg",\r\n          "03.jpg",\r\n          "04.jpg",\r\n          "05.jpg",\r\n          "06.jpg",\r\n          "07.jpg",\r\n          "08.jpg",\r\n          "09.jpg",\r\n          "10.jpg",\r\n          "11.jpg"\r\n        ],\r\n        "descripcion": "Proyecto vial de 8 km de longitud, adicionales a la Avda. Costanera Norte primera etapa (3 km), que se desarrolla íntegramente en tierras de la Municipalidad de Asunción. Completa la funcionalidad vial de acceso y salida dela Franja Costera Norte, que se inicia en el Palacio de Gobierno frente a la bahía y termina en el Jardín Botánico.",\r\n        "fichaTecnica": {\r\n          "ubicacion": "Bañado Norte",\r\n          "superficie": "8 km",\r\n          "fuenteFinanciacion": "FOCEM - Fondos para la Convergencia Estructural del Mercosur.",\r\n          "costoEstimativo": "78.000.000 US$",\r\n          "enteEjecutor": [ "MOPC" ],\r\n          "beneficiarios": "",\r\n          "tipo": [\r\n            "Vial - Movilidad",\r\n            "Espacio Público"\r\n          ]\r\n        }\r\n      },\r\n      {\r\n        "name": "Conexión Avda. Primer Presidente y Ruta Nacional No. 9",\r\n        "images": [\r\n          "12.jpg",\r\n          "13.jpg",\r\n          "14.jpg"\r\n        ],\r\n        "descripcion": "Es un subroyecto de construcción de una interconexión urbana que incluye dos viaductos que totalizan 1.800 metros de longitud para unir la Avda. Costanera Norte con la Avda. Primer Presidente y su empalme correspondiente, que se bifurca en los sentidos de la Ruta Transchaco y la Avda. Ñu Guazu, respectivamente, completando el sistema de accesos desde el norte de la región oriental, a la ciudad de Asunción.\\nEl subproyecto incluye la reconversión urbana de toda la interconexión, previendo las calles de tránsito barrial lento, jardines verticales y espacios verdes, sistema de drenaje en la zona adyacente, mejoramiento del pavimento y señalización de las calles en el área de influencia del proyecto. \\nEl subproyecto incluye la revitalización del Jardín Botánico con programas de restauración de sus casas históricas, nuevos baños públicos, nuevo portal de acceso, circuito cerrado de seguridad, señalética, nueva playa de estacionamiento externa al cerco perimetral, renovación del cerco protector perimetral, entre otras obras. ",\r\n        "fichaTecnica": {\r\n          "ubicacion": "Avenida Primer Presidente",\r\n          "superficie": "2 km",\r\n          "fuenteFinanciacion": "FOCEM",\r\n          "costoEstimativo": "30.000.000 US$",\r\n          "enteEjecutor": [ "MOPC" ],\r\n          "beneficiarios": "",\r\n          "familias": "En identificación",\r\n          "tipo": [\r\n            "Vial - Movilidad",\r\n            "Vial - Ordenamiento urbano",\r\n            "Espacio Público"\r\n          ]\r\n        }\r\n      },\r\n      {\r\n        "name": "Ciclovía Costanera Norte Segunda Etapa",\r\n        "slogan": "Transportándonos con sostenibilidad",\r\n        "images": [\r\n          "15.jpg",\r\n          "16.jpg"\r\n        ],\r\n        "descripcion": "Proyecto de bicisenda dentro del área de dominio de la Avda. Costanera Norte, que acompaña el trazado de la misma. Con esta obra, la Costanera Norte estará iniciando el primer tramo formal de toda una red de ciclovías en la ciudad de Asunción.",\r\n        "fichaTecnica": {\r\n          "ubicacion": "Bañado Norte",\r\n          "superficie": "8 km",\r\n          "fuenteFinanciacion": "Primera Etapa: Fondos del Tesoro Nacional / Segunda Etapa: FOCEM",\r\n          "costoEstimativo": "500.000 US$",\r\n          "enteEjecutor": [ "MOPC" ],\r\n          "beneficiarios": "",\r\n          "familias": "No hay familias en el área",\r\n          "tipo": [\r\n            "Vial - Movilidad",\r\n            "Espacio Público"\r\n          ]\r\n        }\r\n      }\r\n    ],\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "18 km",\r\n      "fuenteFinanciacion": "FOCEM - Fondos para la Convergencia Estructural del Mercosur.",\r\n      "costoEstimativo": "108.500.000 US$",\r\n      "enteEjecutor": [ "MOPC" ],\r\n      "beneficiarios": "",\r\n      "tipo": [\r\n        "Vial - Movilidad",\r\n        "Espacio Público"\r\n      ]\r\n    },\r\n    "color": "#e65100",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "50%",\r\n      "left": "43.6%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.240429,\r\n        -57.589592\r\n      ],\r\n      "parent": true,\r\n      "zoom": 14,\r\n      "center": [\r\n        -25.25543,\r\n        -57.589592\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "right": "10%",\r\n        "width": "16.875rem"\r\n      },\r\n      "arrowStyles": {\r\n        "right": "0.8125rem"\r\n      }\r\n    }\r\n  }
215	Issue	32	6	11857f1b
95	Issue	25	2	-57.627422
96	Issue	25	4	Edgar Valdez
97	Issue	25	5	edgarvaldez99@gmail.com
106	Issue	28	1	-25.283232
107	Issue	28	2	-57.627422
108	Issue	28	4	Edgar Valdez
109	Issue	28	5	edgarvaldez99@gmail.com
110	Issue	29	1	-25.283232
111	Issue	29	2	-57.627422
112	Issue	29	4	Arturo Volpe
113	Issue	29	5	edgarvaldez99@gmail.com
122	Issue	32	1	-25.306519
123	Issue	32	2	-57.65432
124	Issue	32	4	Arturo Volpe
125	Issue	32	5	arturovolpe@gmail.com
140	Issue	36	4	Arturo Volpe
141	Issue	36	5	arturovolpe@gmail.com
142	Issue	37	1	-25.306519
143	Issue	37	2	-57.65432
144	Issue	37	4	Arturo Volpe
145	Issue	37	5	arturovolpe@gmail.com
146	Issue	38	1	-25.306519
147	Issue	38	2	-57.65432
148	Issue	38	4	Edgar Valdez
149	Issue	38	5	edgarvaldez99@gmail.com
216	Issue	31	6	22857f1b
217	Issue	33	6	33b348a4
20	Project	12	3	{\r\n    "id": 12,\r\n    "code": "ecobahia",\r\n    "name": "Eco Bahía I",\r\n    "types": [\r\n      "Urbanización privada"\r\n    ],\r\n    "status": {\r\n      "planification": 2016,\r\n      "bidding": 2018,\r\n      "execution": 2018,\r\n      "finished": 2029\r\n    },\r\n    "slogan": "Rescatando la deuda social",\r\n    "families": {\r\n      "amount": 200,\r\n      "message": "Aproximadamente170 familias residían en el área y 30 en proceso de acuerdo.",\r\n      "popover": ""\r\n    },\r\n    "number": 12,\r\n    "costo": 329000000,\r\n    "images": [\r\n      "01.jpg"\r\n    ],\r\n    "descripcion": "Proyecto de desarrollo de un nuevo barrio urbano de usos mixtos en la costanera norte por medio de un fideicomiso municipal. Del actualmente terreno rellenado, se propone fabricar suelo urbano en tierras degradadas, para la venta a desarrolladores inmobiliarios. De los beneficios del contrato de fiducia se realizará el más importante emprendimiento de inversión social en la Franja Costera, consistente en la consolidación integral y participativa del barrio Chacarita Baja. Este es un proyecto municipal de extensión de redes de servicios sanitarios, electricidad, iluminación, vías seguras, reconstrucción de casas en situación de riesgo y, sobre todo, regularización de la tenencia de tierras.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Franja Costera Norte - Bahía de Asunción - Avenida General Santos y Avenida Costanera José Asunción Flores",\r\n      "superficie": "22 Has (1era: Etapa 10 Has / 2da Etapa: 12 Has)",\r\n      "fuenteFinanciacion": "Municipalidad de Asunción",\r\n      "costoEstimativo": "Relleno: 3.000.000 US$ (Municipalidad de Asunción).\\nUrbanización: 16.000.000 US$ (Inversión pública en infraestructura urbana.\\n310.000.000 US$ (Capital Privado. Superficie estimada de construcción a desarrollarse por el sector privado: 615.000 m2).",\r\n      "enteEjecutor": [ "Municipalidad de Asunción", "APP (Asociación Público Privada)" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "documentos": [\r\n      {"name": "Asunción presentará oportunidades de desarrollo inmobiliario en Chile publicación prensarealestate", "link": "http://prensarealestate.com/asuncion-oportunidades-desarrollo-inmobiliario/"}\r\n    ],\r\n    "color": "#f0b52d",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "35.1%",\r\n      "left": "25.14%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.271378,\r\n        -57.611256\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.275518,\r\n        -57.60861\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "top",\r\n      "styles": {\r\n        "right": "-85%"\r\n      },\r\n      "arrowStyles": {\r\n        "right": "calc(50% - 7px)"\r\n      }\r\n    }\r\n  }
21	Project	13	3	{\r\n    "id": 13,\r\n    "code": "tacumbu",\r\n    "name": "Nuevo Barrio Tacumbú",\r\n    "types": [\r\n      "Urbanización social",\r\n      "Restauración Ambiental",\r\n      "Vial - Movilidad"\r\n    ],\r\n    "status": {\r\n      "planification": 2016,\r\n      "bidding": 2019,\r\n      "execution": 2019,\r\n      "finished": 2025\r\n    },\r\n    "slogan": "Refundando un barrio social",\r\n    "families": {\r\n      "amount": 2600,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 13,\r\n    "costo": 300000000,\r\n    "images": [\r\n      "01.jpeg"\r\n    ],\r\n    "documentos": [\r\n      {"name": "Nuevo Barrio Tacumbú Publicación Itaipú", "link": "https://drive.google.com/open?id=1uiI4YLMGWcWZxbE6rO3_Ivn_KRQxIe-_"},\r\n      {"name": "Proyectan un nuevo barrio modelo para el Bañado Sur_publicación adndigital", "link": "http://www.adndigital.com.py/proyectan-nuevo-barrio-modelo-banado-sur/"},\r\n      {"name": "Itaipú proyecta un nuevo barrio modelo para el Bañado Sur publicación ip", "link": "https://www.ip.gov.py/ip/itaipu-proyecta-un-nuevo-barrio-modelo-para-el-banado-sur/"}\r\n    ],\r\n    "descripcion": "Proyecto de construcción de 2.600 viviendas en 130 hectáreas de tierras rellenadas en la Franja Costera Sur, para relocalizar a los pobladores del área y sacarlos del problema recurrente de las inundaciones y de creación de espacios públicos en las costas del Río Paraguay, de saneamiento ambiental de la laguna Yrupé y un parque lineal de 2.000 metros de longitud. Este proyecto forma parte de un pacto social y político entre la Municipalidad de Asunción, el Gobierno Central y los pobladores del lugar.",\r\n    "componentes": [\r\n      {\r\n        "name": "Bozzano",\r\n        "images": [\r\n          "01.jpeg"\r\n        ],\r\n        "descripcion": "El barrio albergará a 1.000 familias."\r\n      },\r\n      {\r\n        "name": "Tacumbú 1",\r\n        "images": [\r\n          "01.jpeg"\r\n        ],\r\n        "descripcion": "El barrio albergará a 1.000 familias."\r\n      },\r\n      {\r\n        "name": "Tacumbú 2",\r\n        "images": [\r\n          "01.jpeg"\r\n        ],\r\n        "descripcion": "El barrio albergará a 1.600 familias."\r\n      },\r\n      {\r\n        "name": "Barrio Social Sur 1",\r\n        "images": [\r\n          "01.jpeg"\r\n        ],\r\n        "descripcion": "El barrio albergará a 1.600 familias."\r\n      }\r\n    ],\r\n    "fichaTecnica": {\r\n      "ubicacion": "Franja Costera Sur",\r\n      "superficie": "131",\r\n      "fuenteFinanciacion": "Itaipú - MOPC - SENAVITAT - Municipalidad de Asunción -CAMSAT",\r\n      "costoEstimativo": "300.000.000 US$ (Inversión en Infraestructura, Edificios Públicos y Viviendas)",\r\n      "enteEjecutor": [ "Itaipú", "MOPC", "SENAVITAT", "Municipalidad de Asunción" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#a763a7",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "30.4%",\r\n      "left": "84.5%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.306519,\r\n        -57.65432\r\n      ],\r\n      "parent": true,\r\n      "zoom": 15,\r\n      "center": [\r\n        -25.310652,\r\n        -57.649998\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "left": "15%",\r\n        "width": "9.5rem"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.625rem"\r\n      }\r\n    }\r\n  }
22	Project	14	3	{\r\n    "id": 14,\r\n    "code": "chacarita_alta",\r\n    "name": "Consolidación del Barrio Chacarita Alta",\r\n    "types": [\r\n      "Consolidación de barrios"\r\n    ],\r\n    "status": {\r\n      "planification": 2017,\r\n      "bidding": 2018,\r\n      "execution": 2018,\r\n      "finished": 2029\r\n    },\r\n    "slogan": "Cuidando la vida costera alta",\r\n    "families": {\r\n      "amount": 100,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 14,\r\n    "costo": 14000000,\r\n    "images": [\r\n      "01.jpeg"\r\n    ],\r\n    "documentos": [\r\n      {"name": "Consolidación del Barrio Chacarita Alta. Contrato de Préstamo BID", "link": "https://drive.google.com/open?id=1qAliT16SXNcZkw_XuWGOpGx34TxjAKyC"},\r\n      {"name": "Iniciarán relevamiento de terreno y viviendas de Chacarita Alta, publicación  ip.gov.py", "link": "https://www.ip.gov.py/ip/iniciaran-relevamiento-de-terreno-y-viviendas-de-chacarita-alta/"},\r\n      {"name": "Proyectan Transformación integral del Barrio Chacatira Alta en un Proyecto Interinstitucional, publicación MCA", "link": "http://www.asuncion.gov.py/costanera/proyectan-transformacion-integral-del-barrio-chacarita-alta-proyecto-interinstitucional"},\r\n      {"name": "Mejoramiento integral del Barrio Chacarita Alta, pag.senavitat", "link": "https://www.senavitat.gov.py/blog/programas/chacarita-alta/"},\r\n      {"name": "Consolidación del Barrio Chacarita Alta. Propuesta Urbanística de Intervención de Chacarita Alta", "link": "https://drive.google.com/open?id=1Ki9ROSY1NTbEoqh7ArSjM-IYmJqcM3dR"}\r\n    ],\r\n    "descripcion": "Proyecto de consolidación de una de las zonas costeras más antiguas de Asunción, el Barrio Ricardo Brugada en su área no inundable, conocida como Chacarita Alta. El objetivo principal es mejorar la calidad de vida de la gente que vive todavía en condiciones de precariedad en esta zona ribereña. Las obras transformarán la calidad de calles y aceras del vecindario, la provisión formal de servicios básicos: saneamiento (agua potable y cloaca), electricidad, instalaciones de salud y educación, vías de acceso, iluminación, muros de contención para evitar desmoronamientos, gestión de riesgos y seguridad barrial. Se contempla además la regularización de la tenencia de las tierras.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Franja Costera Norte",\r\n      "superficie": "35 Has. (Chacarita Alta 18 Has / Chacarita Alta: 17 Has)",\r\n      "fuenteFinanciacion": "SENAVITAT - BID",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "SENAVITAT" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#d81b60",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "38%",\r\n      "left": "39.5%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.281673,\r\n        -57.624683\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.281673,\r\n        -57.624683\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "top",\r\n      "styles": {\r\n        "left": "10%",\r\n        "width": "9.375rem"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.8125rem"\r\n      }\r\n    }\r\n  }
23	Project	15	3	{\r\n    "id": 15,\r\n    "code": "chacarita_baja",\r\n    "name": "Consolidación del Barrio Chacarita Baja",\r\n    "types": [\r\n      "Consolidación de barrios"\r\n    ],\r\n    "status": {\r\n      "planification": 2017,\r\n      "bidding": 2019,\r\n      "execution": 2019,\r\n      "finished": 2023\r\n    },\r\n    "slogan": "Mejorando la vida en los bajos de la costa",\r\n    "families": {\r\n      "amount": 2000,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 15,\r\n    "costo": 30000000,\r\n    "images": [\r\n      "01.jpeg"\r\n    ],\r\n    "descripcion": "Proyecto de construcción de barrios sociales para consolidar una de las zonas cuya población es una de las más antiguas de las costas de Asunción. Conocido también como Barrio Ricardo Brugada, este proyecto pretende mejorar radicalmente la calidad de vida de la gente que habita el área inundable de la zona. El censo de familias se halla en proceso de elaboración como base para el posterior diseño de protección contra las inundaciones y se tenga listo el proyecto ejecutivo físico-financiero de obras de consolidación del barrio. El paso siguiente será la búsqueda de la financiación municipal directa del emprendimiento como resultado de la venta de tierras urbanizadas del desarrollo inmobiliario (por medio de un fideicomiso) del distrito Eco Bahía I.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Franja Costera Norte",\r\n      "superficie": "35 Has. (Chacarita Alta 18 Has / Chacarita Alta: 17 Has)",\r\n      "fuenteFinanciacion": "Municipalidad de Asunción",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "Municipalidad de Asunción" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#ec407a",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "39.2%",\r\n      "left": "36.35%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.278353,\r\n        -57.623657\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.28275,\r\n        -57.62156\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "bottom",\r\n      "styles": {\r\n        "right": "-73%"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "calc(50% - 8px)"\r\n      }\r\n    }\r\n  }
218	Issue	28	6	99b348da
98	Issue	26	1	-25.283232
99	Issue	26	2	-57.627422
100	Issue	26	4	Arturo Volpe
101	Issue	26	5	arturovolpe@gmail.com
114	Issue	30	1	-25.283232
115	Issue	30	2	-57.627422
116	Issue	30	4	Edgar Valdez
117	Issue	30	5	edgarvaldez99@gmail.com
126	Issue	33	1	-25.306519
127	Issue	33	2	-57.65432
128	Issue	33	4	Edgar Valdez
129	Issue	33	5	edgarvaldez99@gmail.com
130	Issue	34	1	-25.306519
131	Issue	34	2	-57.65432
132	Issue	34	4	Edgar Valdez
133	Issue	34	5	edgarvaldez99@gmail.com
150	Issue	39	1	-25.292269
151	Issue	39	2	-57.659416
152	Issue	39	4	Arturo Volpe
153	Issue	39	5	arturovolpe@gmail.com
24	Project	16	3	{\r\n    "id": 16,\r\n    "code": "cara_cara",\r\n    "name": "Urbanización Cerrito Cará-Cará",\r\n    "types": [\r\n      "Urbanización social",\r\n      "Espacio Público"\r\n    ],\r\n    "status": {\r\n      "planification": 2013,\r\n      "bidding": 2015,\r\n      "execution": 2015,\r\n      "finished": 2019\r\n    },\r\n    "slogan": "Viviendo en un barrio sostenible",\r\n    "families": {\r\n      "amount": 94,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 16,\r\n    "costo": 9600000,\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg"\r\n    ],\r\n    "documentos": [\r\n      {"name": "Urbanización Cerrito Cara Cara_PBC", "link": "https://drive.google.com/open?id=1VysNm2tKDKBYVgzoxTPN069C4Ko36zzt"},\r\n      {"name": "Urbanización Cerrito Cara Cara_Plan de reasentamiento_ Adenda 2.pdf", "link": "https://drive.google.com/open?id=1KilnJjThVVf7HY8y2IouBauDCpjVcbKI"},\r\n      {"name": "Urbanización Cerrito Cara Cara_Video", "link": "http://www.essap.com.py/type/video/"}\r\n    ],\r\n    "descripcion": "La Urbanización Cerrito Cará-Cará incluye viviendas para unas 94 familias que optaron por permanecer en el lugar, una de las tres opciones de reasentamiento que se les ofreció, a raíz de la necesidad de su desplazamiento para dar lugar a la construcción de la primera planta de tratamiento de efluentes cloacales de Asunción, la Planta Bella Vista.  La construcción de la urbanización está a cargo de la ESSAP y prevé, además, un plan de desarrollo comunitario con un centro propio, polideportivo y un área de recreación para niños. Actualmente, mientras se culmina la urbanización, todas las familias están siendo asistidas con un estipendio para pago de acomodación temporal por la ESSAP.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Distrito de Santísima Trinidad, Bañado Norte, Barrio Cerrito Cara Cara, margen derecha del arroyo Mburicao, predio PTAR",\r\n      "superficie": "3,2 Has",\r\n      "fuenteFinanciacion": "Préstamo Banco Internacional de Reconstrucción y Fomento (BIRF) y contrapartida de la ESSAP.",\r\n      "costoEstimativo": "Estimativamente ₲ 54.908.773.103 (incluye Obras de Relleno Hidráulico y Convencional y de Construcción de Viviendas Unifamiliares de Carácter Social - Fase I PTAR Bella Vista)",\r\n      "enteEjecutor": [ "ESSAP" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#ad1457",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "31.3%",\r\n      "left": "2.5%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.251997,\r\n        -57.59144\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.252996,\r\n        -57.59144\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "top",\r\n      "styles": {\r\n        "left": "10%"\r\n      },\r\n      "arrowStyles": {\r\n        "left": "0.625rem"\r\n      }\r\n    }\r\n  }
25	Project	17	3	{\r\n    "id": 17,\r\n    "code": "barrio_cerrito",\r\n    "name": "Barrio Cerrito",\r\n    "types": [\r\n      "Urbanización social",\r\n      "Espacio Público",\r\n      "Ordenamiento urbano"\r\n    ],\r\n    "status": {\r\n      "planification": 2018,\r\n      "bidding": 2019,\r\n      "execution": 2019,\r\n      "finished": 2022\r\n    },\r\n    "slogan": "Encontrando juntos soluciones habitacionales",\r\n    "families": {\r\n      "amount": 300,\r\n      "message": "",\r\n      "popover": ""\r\n    },\r\n    "number": 17,\r\n    "costo": 12000000,\r\n    "images": [\r\n      "01.jpg",\r\n      "02.jpg",\r\n      "03.jpg"\r\n    ],\r\n    "documentos": [\r\n      {"name": "Barrio Cerrito_informe sobre pedido de vecinos barrio cerrito, essap", "link": "https://drive.google.com/open?id=1pOcH2CFKOa7E3UKy0C6Q-mFatRnLGB81"},\r\n      {"name": "Validan censo realizado a 345 familias del barrio Cerrito, publicación essap", "link": "http://www.essap.com.py/5d83ad1b49a1173a6a4dfa61097eeec1/"}\r\n    ],\r\n    "descripcion": "Este Barrio responde a una de las propuestas de inclusión social en la urbanización de la Franja Costera de los planes de ASU VIVA. Particularmente, la construcción de urbanizaciones con viviendas de interés social para familias que se encuentran en asentamientos y zonas de futuras obras de infraestructura.\\nEl Barrio será diseñado como un barrio más de la ciudad formal como parte del entorno y de la vida ciudadana, que genera oportunidades de inclusión social.\\nEste barrio en particular será destinado a alrededor de 300 familias que contarán con viviendas dignas con todos los servicios, en una cota no inundable y concentrando la superficie del barrio en un sector cercano a sus asentamientos actuales, para no perder arraigo al sitio.",\r\n    "fichaTecnica": {\r\n      "ubicacion": "Bañado Norte",\r\n      "superficie": "4",\r\n      "fuenteFinanciacion": "En definición",\r\n      "costoEstimativo": "",\r\n      "enteEjecutor": [ "MOPC", "SENAVITAT" ],\r\n      "beneficiarios": ""\r\n    },\r\n    "color": "#880e4f",\r\n    "markerType": "",\r\n    "markerPositions": {\r\n      "top": "31%",\r\n      "left": "5.2%"\r\n    },\r\n    "mapProperties": {\r\n      "mapIconPosition": [\r\n        -25.251997,\r\n        -57.59144\r\n      ],\r\n      "parent": true,\r\n      "zoom": 16,\r\n      "center": [\r\n        -25.252996,\r\n        -57.59144\r\n      ]\r\n    },\r\n    "popover": {\r\n      "position": "right",\r\n      "styles": {\r\n        "top": "-10%",\r\n        "left": "110%",\r\n        "width": "5.8125rem"\r\n      },\r\n      "arrowStyles": {}\r\n    }\r\n  }
26	Issue	8	1	-25.292269
27	Issue	8	2	-57.659416
28	Issue	8	4	edgar
29	Issue	8	5	edgarvaldez99@gmail.com
30	Issue	9	1	-25.292269
31	Issue	9	2	-57.659416
32	Issue	9	4	Edgar Valdez
33	Issue	9	5	edgarvaldez99@gmail.com
34	Issue	10	1	-25.292269
35	Issue	10	2	-57.659416
36	Issue	10	4	Edgar Faria
37	Issue	10	5	edgar.faria@hotmail.com
38	Issue	11	1	-25.292269
39	Issue	11	2	-57.659416
40	Issue	11	4	Edgar Valdez
41	Issue	11	5	edgarvaldez99@gmail.com
42	Issue	12	1	-25.292269
43	Issue	12	2	-57.659416
44	Issue	12	4	Arturo Volpe
45	Issue	12	5	arturovolpe@gmail.com
46	Issue	13	1	-25.292269
47	Issue	13	2	-57.659416
48	Issue	13	4	Arturo Volpe
49	Issue	13	5	arturovolpe@gmail.com
50	Issue	14	1	-25.292269
51	Issue	14	2	-57.659416
52	Issue	14	4	aasdfsadfsadf
53	Issue	14	5	arturovolpe@gmail.com
54	Issue	15	1	-25.292269
55	Issue	15	2	-57.659416
56	Issue	15	4	asfdsadfsadf
57	Issue	15	5	arturovolpe@gmail.com
58	Issue	16	1	-25.292269
59	Issue	16	2	-57.659416
60	Issue	16	4	asgdagdsa gsad
61	Issue	16	5	arturovolpe@gmail.com
62	Issue	17	1	-25.292269
63	Issue	17	2	-57.659416
64	Issue	17	4	asdfdsafdsaf
65	Issue	17	5	arturovolpe@gmail.com
66	Issue	18	1	-25.306519
67	Issue	18	2	-57.65432
68	Issue	18	4	asdfsafddsaf
69	Issue	18	5	arturovolpe@gmail.com
70	Issue	19	1	-25.306519
71	Issue	19	2	-57.65432
72	Issue	19	4	asdfsadf
73	Issue	19	5	arturovolpe@gmail.com
74	Issue	20	1	-25.306519
75	Issue	20	2	-57.65432
76	Issue	20	4	afdsadsfadsf
77	Issue	20	5	arturovolpe@gmail.com
78	Issue	21	1	-25.283232
79	Issue	21	2	-57.627422
80	Issue	21	4	Arturo Volpe
81	Issue	21	5	arturovolpe@gmail.com
82	Issue	22	1	-25.283232
83	Issue	22	2	-57.627422
84	Issue	22	4	Arturo Volpe
85	Issue	22	5	arturovolpe@gmail.com
86	Issue	23	1	-25.283232
87	Issue	23	2	-57.627422
88	Issue	23	4	Arturo Volpe
89	Issue	23	5	arturovolpe@gmail.com
90	Issue	24	1	-25.283232
91	Issue	24	2	-57.627422
94	Issue	25	1	-25.283232
154	Issue	40	1	-25.283232
155	Issue	40	2	-57.627422
156	Issue	40	4	Edgar Valdez
157	Issue	40	5	edgarvaldez99@gmail.com
159	Issue	41	1	-25.283232
160	Issue	41	2	-57.627422
161	Issue	41	4	Edgar Valdez
162	Issue	41	5	edgarvaldez99@gmail.com
164	Issue	42	1	-25.283232
165	Issue	42	2	-57.627422
166	Issue	42	4	Edgar Valdez
167	Issue	42	5	edgarvaldez99@gmail.com
168	Issue	42	6	b77dd44a
169	Issue	43	1	-25.283232
170	Issue	43	2	-57.627422
171	Issue	43	4	Edgar Valdez
172	Issue	43	5	edgarvaldez99@gmail.com
174	Issue	44	1	-25.248075
175	Issue	44	2	-57.603947
176	Issue	44	4	Edgar Valdez
177	Issue	44	5	edgarvaldez99@gmail.com
178	Issue	44	6	e9b348a4
179	Issue	45	1	-25.248075
180	Issue	45	2	-57.603947
181	Issue	45	4	Edgar Valdez
182	Issue	45	5	edgarvaldez99@gmail.com
183	Issue	45	6	6e857f1b
184	Issue	46	1	-25.271378
185	Issue	46	2	-57.611256
186	Issue	46	4	Arturo Volpe
187	Issue	46	5	arturovolpe@gmail.com
188	Issue	46	6	d69613c5
189	Issue	47	1	-25.271378
190	Issue	47	2	-57.611256
191	Issue	47	4	Edgar Valdez
192	Issue	47	5	edgarvaldez99@gmail.com
193	Issue	47	6	34527ca0
194	Issue	48	1	-25.271378
195	Issue	48	2	-57.611256
196	Issue	48	4	Edgar Valdez
197	Issue	48	5	edgarvaldez99@gmail.com
198	Issue	48	6	725fb412
199	Issue	49	1	-25.271378
200	Issue	49	2	-57.611256
201	Issue	49	4	Edgar Valdez
202	Issue	49	5	edgarvaldez99@gmail.com
203	Issue	49	6	37312158
204	Issue	50	1	-25.251997
205	Issue	50	2	-57.59144
206	Issue	50	4	Edgar Valdez
207	Issue	50	5	edgarvaldez99@gmail.com
208	Issue	50	6	d9a4a429
158	Issue	40	6	6e857f7b
163	Issue	41	6	e8b348a4
173	Issue	43	6	e9b348da
209	Issue	39	6	1e857f1b
210	Issue	38	6	2e857f1b
211	Issue	37	6	3e857f1b
212	Issue	36	6	
213	Issue	35	6	5e857f1b
214	Issue	34	6	9a857f1b
219	Issue	30	6	88857f7b
220	Issue	29	6	e95558da
221	Issue	27	6	1e857999
222	Issue	26	6	e8b34888
223	Issue	25	6	e93338a4
224	Issue	51	1	-25.25232
225	Issue	51	2	-57.61309
226	Issue	51	4	Edgar Valdez
227	Issue	51	5	edgarvaldez99@gmail.com
228	Issue	51	6	92e07f6f
\.


--
-- Name: custom_values_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.custom_values_id_seq', 228, true);


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.documents (id, project_id, category_id, title, description, created_on) FROM stdin;
\.


--
-- Name: documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.documents_id_seq', 1, false);


--
-- Data for Name: email_addresses; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.email_addresses (id, user_id, address, is_default, notify, created_on, updated_on) FROM stdin;
1	1	admin@example.net	t	t	2018-08-31 14:10:04.263107	2018-08-31 14:10:04.263107
2	5	sinpermisos@sinpermisos.com	t	t	2018-08-31 14:13:18.528002	2018-08-31 14:13:18.528002
3	6	express@express.com.py	t	t	2018-09-03 11:43:11.034578	2018-09-03 11:43:11.034578
4	24	edgarvaldez99@gmail.com	t	t	2018-09-12 15:08:01.623573	2018-09-12 15:08:01.623573
\.


--
-- Name: email_addresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.email_addresses_id_seq', 4, true);


--
-- Data for Name: enabled_modules; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.enabled_modules (id, project_id, name) FROM stdin;
1	1	issue_tracking
11	2	issue_tracking
12	3	issue_tracking
13	4	issue_tracking
14	5	issue_tracking
15	6	issue_tracking
16	7	issue_tracking
17	8	issue_tracking
18	9	issue_tracking
19	10	issue_tracking
20	11	issue_tracking
21	12	issue_tracking
22	13	issue_tracking
23	14	issue_tracking
24	15	issue_tracking
25	16	issue_tracking
26	17	issue_tracking
\.


--
-- Name: enabled_modules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.enabled_modules_id_seq', 26, true);


--
-- Data for Name: enumerations; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.enumerations (id, name, "position", is_default, type, active, project_id, parent_id, position_name) FROM stdin;
1	Baja	1	f	IssuePriority	t	\N	\N	lowest
2	Normal	2	t	IssuePriority	t	\N	\N	default
3	Alta	3	f	IssuePriority	t	\N	\N	high3
4	Urgente	4	f	IssuePriority	t	\N	\N	high2
5	Inmediata	5	f	IssuePriority	t	\N	\N	highest
6	Documentación de usuario	1	f	DocumentCategory	t	\N	\N	\N
7	Documentación técnica	2	f	DocumentCategory	t	\N	\N	\N
8	Diseño	1	f	TimeEntryActivity	t	\N	\N	\N
9	Desarrollo	2	f	TimeEntryActivity	t	\N	\N	\N
\.


--
-- Name: enumerations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.enumerations_id_seq', 9, true);


--
-- Data for Name: groups_users; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.groups_users (group_id, user_id) FROM stdin;
8	24
23	24
22	24
16	24
20	24
21	24
17	24
7	24
18	24
12	24
9	24
10	24
11	24
14	24
13	24
19	24
15	24
\.


--
-- Data for Name: import_items; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.import_items (id, import_id, "position", obj_id, message) FROM stdin;
\.


--
-- Name: import_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.import_items_id_seq', 1, false);


--
-- Data for Name: imports; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.imports (id, type, user_id, filename, settings, total_items, finished, created_at, updated_at) FROM stdin;
\.


--
-- Name: imports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.imports_id_seq', 1, false);


--
-- Data for Name: issue_categories; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.issue_categories (id, project_id, name, assigned_to_id) FROM stdin;
\.


--
-- Name: issue_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.issue_categories_id_seq', 1, false);


--
-- Data for Name: issue_relations; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.issue_relations (id, issue_from_id, issue_to_id, relation_type, delay) FROM stdin;
\.


--
-- Name: issue_relations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.issue_relations_id_seq', 1, false);


--
-- Data for Name: issue_statuses; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.issue_statuses (id, name, is_closed, "position", default_done_ratio) FROM stdin;
1	Pendiente	f	1	\N
2	En revisión	f	2	\N
6	Rechazada	t	4	\N
3	Resuelta	t	3	\N
\.


--
-- Name: issue_statuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.issue_statuses_id_seq', 6, true);


--
-- Data for Name: issues; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.issues (id, tracker_id, project_id, subject, description, due_date, category_id, status_id, assigned_to_id, priority_id, fixed_version_id, author_id, lock_version, created_on, updated_on, start_date, done_ratio, estimated_hours, parent_id, root_id, lft, rgt, is_private, closed_on) FROM stdin;
42	1	3	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda asdfasdfdsa	\N	\N	6	\N	2	\N	1	11	2018-09-12 10:04:26.672732	2018-09-12 16:20:02.693196	2018-09-12	10	\N	\N	42	1	2	t	2018-09-12 16:20:02.693196
11	1	1	Reclamo de Edgar Valdez	asdfasdf asdfdsaf adsfasd	\N	\N	3	\N	2	\N	1	3	2018-09-10 14:53:26.953023	2018-09-11 11:28:23.523702	2018-09-10	100	\N	\N	11	1	2	t	2018-09-11 11:26:16.535194
17	1	1	Reclamo de Celia Giménez	asfdasfsadfdsaf	\N	\N	1	1	2	\N	1	3	2018-09-10 15:25:26.735182	2018-09-11 13:25:55.948488	2018-09-10	0	\N	\N	17	1	2	t	\N
16	1	1	Reclamo de Juan Pane	asdfsadf312414214	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:23:34.273956	2018-09-11 13:26:11.51693	2018-09-10	0	\N	\N	16	1	2	t	\N
15	1	1	Reclamo de Arturo Volpe	dsafdsafadsfdsafsdaf	\N	\N	1	\N	2	\N	1	8	2018-09-10 15:23:20.97067	2018-09-11 13:26:44.965989	2018-09-10	70	\N	\N	15	1	2	t	\N
14	1	1	Reclamo de Edgar Valdez	fdsafdsafdsa f 123123213	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:22:40.23432	2018-09-11 13:27:01.637241	2018-09-10	0	\N	\N	14	1	2	t	\N
10	1	1	Reclamo de Edgar Faria	La tapa triangular del desagüe cayo dentro del mismo y está sin tapa	\N	\N	1	1	2	\N	1	1	2018-09-10 11:34:59.857873	2018-09-11 13:51:03.731344	2018-09-10	10	\N	\N	10	1	2	t	\N
43	1	3	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda un carajen	\N	\N	1	\N	2	\N	1	2	2018-09-12 15:46:21.597338	2018-09-13 14:19:38.672687	2018-09-12	10	\N	\N	43	1	2	t	\N
45	1	5	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda nada 	\N	\N	1	\N	2	\N	1	2	2018-09-12 16:21:07.883058	2018-09-13 10:47:32.824759	2018-09-12	10	\N	\N	45	1	2	t	\N
44	1	5	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda nada	\N	\N	1	\N	2	\N	1	2	2018-09-12 16:02:26.823017	2018-09-13 10:47:49.684544	2018-09-12	10	\N	\N	44	1	2	t	\N
8	1	8	Reclamo de edgar	no anda	\N	\N	1	\N	2	\N	1	0	2018-09-07 09:53:14.996067	2018-09-07 09:53:14.996067	2018-09-07	0	\N	\N	8	1	2	t	\N
9	1	1	Reclamo de Edgar Valdez	No anda un carajo	\N	\N	1	\N	2	\N	1	0	2018-09-10 09:27:06.595885	2018-09-10 09:27:06.595885	2018-09-10	0	\N	\N	9	1	2	t	\N
12	1	1	Reclamo de Arturo Volpe	sadfdsafdsafadsfdsaffdsasadfdsaf	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:10:03.617152	2018-09-10 15:10:03.617152	2018-09-10	0	\N	\N	12	1	2	t	\N
13	1	1	Reclamo de Arturo Volpe	afdsdsafdsafdsa 2 32 423 432	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:20:58.877966	2018-09-10 15:20:58.877966	2018-09-10	0	\N	\N	13	1	2	t	\N
47	1	12	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	777846467897897897	\N	\N	1	\N	2	\N	1	2	2018-09-13 11:41:28.412272	2018-09-13 14:18:59.386078	2018-09-13	10	\N	\N	47	1	2	t	\N
18	1	13	Reclamo de asdfsafddsaf	asdfasfdsaf	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:27:26.633268	2018-09-10 15:27:26.633268	2018-09-10	0	\N	\N	18	1	2	t	\N
19	1	13	Reclamo de asdfsadf	dsafsadfdsafdsaf	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:28:08.529422	2018-09-10 15:28:08.529422	2018-09-10	0	\N	\N	19	1	2	t	\N
46	1	12	Reclamo de Arturo Volpe - arturovolpe@gmail.com	No anda un carajo	\N	\N	1	\N	2	\N	1	2	2018-09-13 11:39:05.382293	2018-09-13 14:19:15.487886	2018-09-13	10	\N	\N	46	1	2	t	\N
20	1	13	Reclamo de afdsadsfadsf	asdfdsafsadfsadfdsafdsafdsaf	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:28:55.08441	2018-09-10 15:28:55.08441	2018-09-10	0	\N	\N	20	1	2	t	\N
21	1	3	Reclamo de Arturo Volpe	adssfdsafsafd	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:33:21.002458	2018-09-10 15:33:21.002458	2018-09-10	0	\N	\N	21	1	2	t	\N
22	1	3	Reclamo de Arturo Volpe	asdfsadfadsf	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:34:19.06489	2018-09-10 15:34:19.06489	2018-09-10	0	\N	\N	22	1	2	t	\N
49	1	12	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda nada	\N	\N	1	\N	2	\N	1	2	2018-09-13 12:09:17.512616	2018-09-13 14:18:15.308798	2018-09-13	10	\N	\N	49	1	2	t	\N
23	1	3	Reclamo de Arturo Volpe	asdfasfdsaf	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:36:01.201248	2018-09-10 15:36:01.201248	2018-09-10	0	\N	\N	23	1	2	t	\N
40	1	3	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda asdfasdfas	\N	\N	1	\N	2	\N	1	1	2018-09-12 10:00:18.077573	2018-09-13 14:05:50.053668	2018-09-12	0	\N	\N	40	1	2	t	\N
24	1	3	Reclamo de Edgar Valdez	asfasdfsadfsadfasdfdsa	\N	\N	1	\N	2	\N	1	0	2018-09-10 15:36:56.607236	2018-09-10 15:36:56.607236	2018-09-10	0	\N	\N	24	1	2	t	\N
41	1	3	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda adsfasdfsadf	\N	\N	1	\N	2	\N	1	1	2018-09-12 10:03:14.821607	2018-09-13 14:06:09.705329	2018-09-12	0	\N	\N	41	1	2	t	\N
51	1	2	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	Hola, escribo para reclamar sobre un bache hecho por un colectivo	\N	\N	3	\N	2	\N	1	2	2018-09-14 08:07:57.168754	2018-09-14 17:11:03.790041	2018-09-14	100	\N	\N	51	1	2	t	2018-09-14 17:11:03.790041
38	1	13	Reclamo de Edgar Valdez	sadfdsafdsafdsafdsafdsafadsfadsf	\N	\N	1	\N	2	\N	1	1	2018-09-10 16:07:03.223711	2018-09-13 14:09:29.185003	2018-09-10	10	\N	\N	38	1	2	t	\N
32	1	13	Reclamo de Arturo Volpe	asdasdsadasdasd	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:55:33.832897	2018-09-13 14:12:25.427945	2018-09-10	10	\N	\N	32	1	2	t	\N
37	1	13	Reclamo de Arturo Volpe	sadfasdfdsa fads fdsa fdsa fdsa	\N	\N	1	\N	2	\N	1	2	2018-09-10 16:06:16.015593	2018-09-13 14:10:18.758514	2018-09-10	10	\N	\N	37	1	2	t	\N
36	1	13	Reclamo de Arturo Volpe	asfasdfdsa fdsa fdsa fdsaf 	\N	\N	1	\N	2	\N	1	1	2018-09-10 16:04:19.192995	2018-09-13 14:10:37.315705	2018-09-10	10	\N	\N	36	1	2	t	\N
35	1	13	Reclamo de Arturo Volpe	asfdasfdsadfasdfdsafdsafdsafdsaf	\N	\N	1	\N	2	\N	1	2	2018-09-10 16:01:00.180156	2018-09-13 14:11:02.924195	2018-09-10	10	\N	\N	35	1	2	t	\N
34	1	13	Reclamo de Edgar Valdez	sadsadsa dsa dsa dsa das das das	\N	\N	1	\N	2	\N	1	2	2018-09-10 15:58:32.692034	2018-09-13 14:11:51.241875	2018-09-10	10	\N	\N	34	1	2	t	\N
31	1	13	Reclamo de Arturo Volpe	asfdsadfasdfadsf	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:49:33.411023	2018-09-13 14:13:30.271978	2018-09-10	10	\N	\N	31	1	2	t	\N
33	1	13	Reclamo de Edgar Valdez	sadfsadfdsafsadfsadfdsaf	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:57:12.478411	2018-09-13 14:14:05.628366	2018-09-10	10	\N	\N	33	1	2	t	\N
28	1	3	Reclamo de Edgar Valdez	asfddsafasdfsadfdsaf	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:41:38.000023	2018-09-13 14:14:46.427455	2018-09-10	10	\N	\N	28	1	2	t	\N
30	1	3	Reclamo de Edgar Valdez	asdfsadfsadfadsfasdfasdf	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:43:09.621089	2018-09-13 14:15:22.165763	2018-09-10	10	\N	\N	30	1	2	t	\N
29	1	3	Reclamo de Arturo Volpe	asdfsadfsadfasdfdsaf	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:42:58.64634	2018-09-13 14:15:51.175858	2018-09-10	10	\N	\N	29	1	2	t	\N
27	1	3	Reclamo de Arturo Volpe	asfsadfsadfadsfadsf	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:41:01.611523	2018-09-13 14:16:21.292181	2018-09-10	10	\N	\N	27	1	2	t	\N
26	1	3	Reclamo de Arturo Volpe	ASDASDSADASDASDASDASD	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:40:04.151068	2018-09-13 14:16:49.765035	2018-09-10	10	\N	\N	26	1	2	t	\N
25	1	3	Reclamo de Edgar Valdez	edgar se las come	\N	\N	1	\N	2	\N	1	1	2018-09-10 15:39:37.253746	2018-09-13 14:17:18.595783	2018-09-10	0	\N	\N	25	1	2	t	\N
50	1	17	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda nada	\N	\N	1	\N	2	\N	1	2	2018-09-13 13:15:52.236622	2018-09-13 14:17:57.864298	2018-09-13	10	\N	\N	50	1	2	t	\N
48	1	12	Reclamo de Edgar Valdez - edgarvaldez99@gmail.com	No anda nada	\N	\N	1	\N	2	\N	1	2	2018-09-13 11:49:14.377164	2018-09-13 14:18:40.333558	2018-09-13	10	\N	\N	48	1	2	t	\N
39	1	1	Reclamo de Arturo Volpe	sper test 2	\N	\N	3	\N	2	\N	1	2	2018-09-10 16:07:40.290271	2018-09-14 17:07:56.380721	2018-09-10	10	\N	\N	39	1	2	t	2018-09-14 17:07:56.380721
\.


--
-- Name: issues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.issues_id_seq', 51, true);


--
-- Data for Name: journal_details; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.journal_details (id, journal_id, property, prop_key, old_value, value) FROM stdin;
1	2	attr	assigned_to_id	\N	1
3	4	attr	done_ratio	0	10
4	5	attr	done_ratio	10	20
6	7	attr	done_ratio	20	30
7	8	attr	done_ratio	30	40
12	13	attr	done_ratio	40	50
15	16	attr	done_ratio	50	60
16	17	attr	done_ratio	60	70
17	18	attr	done_ratio	0	10
18	19	attr	status_id	1	6
19	20	attr	status_id	6	3
20	20	attr	done_ratio	10	100
21	21	attr	subject	Reclamo de asdfdsafdsaf	Reclamo de Celia Giménez
22	22	attr	subject	Reclamo de asgdagdsa gsad	Reclamo de Juan Pane
23	23	attr	subject	Reclamo de asfdsadfsadf	Reclamo de Arturo Volpe
24	24	attr	subject	Reclamo de aasdfsadfsadf	Reclamo de Edgar Valdez
25	25	attr	assigned_to_id	\N	1
26	25	attr	done_ratio	0	10
28	27	attr	done_ratio	0	10
29	28	attr	done_ratio	10	20
32	31	attr	done_ratio	20	50
33	32	attr	done_ratio	50	60
34	33	attr	done_ratio	60	70
36	35	attr	done_ratio	70	80
38	37	attr	done_ratio	80	90
39	38	attr	done_ratio	90	0
47	46	attr	done_ratio	0	10
48	47	attr	status_id	1	6
49	48	attr	done_ratio	0	10
50	49	attr	done_ratio	0	10
51	50	cf	6		6e857f7b
52	51	cf	6		e8b348a4
53	52	cf	6		e9b348da
54	53	attr	done_ratio	0	10
55	53	cf	6		1e857f1b
56	54	attr	done_ratio	0	10
57	54	cf	6		2e857f1b
58	55	cf	6		3e857f1b
59	56	attr	done_ratio	0	10
60	57	attr	done_ratio	0	10
61	58	cf	6		5e857f1b
62	59	attr	done_ratio	0	10
63	60	cf	6		9a857f1b
64	61	attr	done_ratio	0	10
65	62	attr	done_ratio	0	10
66	62	cf	6		11857f1b
67	63	attr	done_ratio	0	10
68	63	cf	6		22857f1b
69	64	attr	done_ratio	0	10
70	64	cf	6		33b348a4
71	65	attr	done_ratio	0	10
72	65	cf	6		99b348da
73	66	attr	done_ratio	0	10
74	66	cf	6		88857f7b
75	67	attr	done_ratio	0	10
76	67	cf	6		e95558da
77	68	attr	done_ratio	0	10
78	68	cf	6		1e857999
79	69	attr	done_ratio	0	10
80	69	cf	6		e8b34888
81	70	cf	6		e93338a4
82	71	attr	done_ratio	0	10
83	72	attr	done_ratio	0	10
84	73	attr	done_ratio	0	10
85	74	attr	done_ratio	0	10
86	75	attr	done_ratio	0	10
87	76	attr	done_ratio	0	10
88	77	attr	status_id	1	3
89	78	attr	status_id	1	3
90	78	attr	done_ratio	0	100
\.


--
-- Name: journal_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.journal_details_id_seq', 90, true);


--
-- Data for Name: journals; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.journals (id, journalized_id, journalized_type, user_id, notes, created_on, private_notes) FROM stdin;
1	17	Issue	1	NOTASASSSS	2018-09-10 17:20:05.974996	f
2	17	Issue	1	NOTASS	2018-09-10 17:21:44.126158	f
4	15	Issue	1	Hola que tal	2018-09-11 08:00:50.272491	f
5	15	Issue	1	Nota 2	2018-09-11 09:34:59.681393	f
7	15	Issue	1	Nota 3	2018-09-11 09:49:13.247654	f
8	15	Issue	1	Nota 4	2018-09-11 09:49:36.155719	f
13	15	Issue	1	Nota 5	2018-09-11 10:14:14.889312	f
16	15	Issue	1	Nota 6	2018-09-11 10:21:19.755798	f
17	15	Issue	1	Notaasdfkaaaaaaaaaaaaa\r\nAalksjdflkasfdlkjsalkjfdlksjdf\r\nçasdkjflaksjfdlkjsalkdfjlkasjdflk \r\nÇasjflkjsalfdhjashfadfad	2018-09-11 10:30:49.551839	f
18	11	Issue	1	Nota 2	2018-09-11 10:57:38.23284	f
19	11	Issue	1	Su pedido no tiene sentido	2018-09-11 11:26:16.546806	f
20	11	Issue	1	Ha sido resuelta 	2018-09-11 11:28:23.542759	f
21	17	Issue	1		2018-09-11 13:25:55.952208	f
22	16	Issue	1		2018-09-11 13:26:11.521172	f
23	15	Issue	1		2018-09-11 13:26:44.972212	f
24	14	Issue	1		2018-09-11 13:27:01.642727	f
25	10	Issue	1	Asignado al encargado	2018-09-11 13:51:03.743971	f
27	42	Issue	1		2018-09-12 10:22:58.166312	f
28	42	Issue	1		2018-09-12 11:19:52.452026	f
31	42	Issue	1		2018-09-12 11:46:27.743126	f
32	42	Issue	1		2018-09-12 11:59:15.437327	f
33	42	Issue	1		2018-09-12 15:02:50.126801	f
35	42	Issue	1		2018-09-12 15:11:34.857586	f
37	42	Issue	1		2018-09-12 15:36:04.090871	f
38	42	Issue	1		2018-09-12 15:43:19.996429	f
46	42	Issue	1		2018-09-12 16:19:30.281509	f
47	42	Issue	1	No entedemos su reclamo\r\n\r\nSaludos.	2018-09-12 16:20:02.697448	f
48	45	Issue	1	Avance 1	2018-09-13 10:47:32.839877	f
49	44	Issue	1	Primer Avance	2018-09-13 10:47:49.687882	f
50	40	Issue	1		2018-09-13 14:05:50.05817	f
51	41	Issue	1		2018-09-13 14:06:09.709872	f
52	43	Issue	1		2018-09-13 14:06:54.52661	f
53	39	Issue	1	A 1	2018-09-13 14:08:16.862237	f
54	38	Issue	1	B 1	2018-09-13 14:09:29.189994	f
55	37	Issue	1	C 3	2018-09-13 14:10:03.721776	f
56	37	Issue	1		2018-09-13 14:10:18.762205	f
57	36	Issue	1	D 1	2018-09-13 14:10:37.322443	f
58	35	Issue	1	F 1	2018-09-13 14:10:57.049691	f
59	35	Issue	1		2018-09-13 14:11:02.929297	f
60	34	Issue	1	G 1	2018-09-13 14:11:38.375884	f
61	34	Issue	1		2018-09-13 14:11:51.245385	f
62	32	Issue	1	H 1	2018-09-13 14:12:25.436738	f
63	31	Issue	1	J 1	2018-09-13 14:13:30.277975	f
64	33	Issue	1	K 1	2018-09-13 14:14:05.637955	f
65	28	Issue	1	L 1	2018-09-13 14:14:46.437192	f
66	30	Issue	1	Ñ 1	2018-09-13 14:15:22.170985	f
67	29	Issue	1	Q 1	2018-09-13 14:15:51.182333	f
68	27	Issue	1	W 1	2018-09-13 14:16:21.299788	f
69	26	Issue	1	R 1	2018-09-13 14:16:49.773187	f
70	25	Issue	1	T 1	2018-09-13 14:17:18.603882	f
71	50	Issue	1	Y 1	2018-09-13 14:17:57.871952	f
72	49	Issue	1	U 1	2018-09-13 14:18:15.31476	f
73	48	Issue	1	I 1	2018-09-13 14:18:40.339618	f
74	47	Issue	1	O 1	2018-09-13 14:18:59.389418	f
75	46	Issue	1	Z 1	2018-09-13 14:19:15.491664	f
76	43	Issue	1		2018-09-13 14:19:38.676294	f
77	39	Issue	1		2018-09-14 17:07:56.388401	f
78	51	Issue	1	Su reclamo ha sido aceptado y estamos trabajando en la solución.\r\n\r\nGracias por su colaboración	2018-09-14 17:11:03.796222	f
\.


--
-- Name: journals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.journals_id_seq', 78, true);


--
-- Data for Name: member_roles; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.member_roles (id, member_id, role_id, inherited_from) FROM stdin;
1	1	6	\N
2	2	3	\N
3	2	5	\N
4	2	6	\N
5	3	3	\N
6	3	5	\N
7	3	6	\N
8	4	3	\N
9	4	5	\N
10	4	6	\N
11	5	3	\N
12	5	5	\N
13	5	6	\N
14	6	3	\N
15	6	5	\N
16	6	6	\N
17	7	3	\N
18	7	5	\N
19	7	6	\N
20	8	3	\N
21	8	6	\N
22	9	3	\N
23	9	5	\N
24	9	6	\N
25	10	3	\N
26	10	5	\N
27	10	6	\N
28	11	5	\N
29	11	6	\N
30	12	3	\N
31	12	5	\N
32	12	6	\N
33	13	3	\N
34	13	5	\N
35	13	6	\N
36	14	3	\N
37	14	5	\N
38	14	6	\N
39	15	3	\N
40	15	5	\N
41	15	6	\N
42	16	3	\N
43	16	5	\N
44	16	6	\N
45	17	3	\N
46	17	5	\N
47	17	6	\N
48	18	3	\N
49	18	5	\N
50	18	6	\N
51	19	3	8
52	19	5	9
53	19	6	10
54	20	3	11
55	20	5	12
56	20	6	13
57	21	3	45
58	21	5	46
59	21	6	47
60	22	3	20
61	22	6	21
62	23	3	14
63	23	5	15
64	23	6	16
65	24	3	17
66	24	5	18
67	24	6	19
68	25	3	2
69	25	5	3
70	25	6	4
71	26	3	5
72	26	5	6
73	26	6	7
74	27	3	22
75	27	5	23
76	27	6	24
77	28	3	36
78	28	5	37
79	28	6	38
80	29	5	28
81	29	6	29
82	30	3	30
83	30	5	31
84	30	6	32
85	31	3	33
86	31	5	34
87	31	6	35
88	32	3	42
89	32	5	43
90	32	6	44
91	33	3	39
92	33	5	40
93	33	6	41
94	34	3	25
95	34	5	26
96	34	6	27
97	35	3	48
98	35	5	49
99	35	6	50
\.


--
-- Name: member_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.member_roles_id_seq', 99, true);


--
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.members (id, user_id, project_id, created_on, mail_notification) FROM stdin;
1	6	1	2018-09-03 11:58:49.756504	f
2	17	11	2018-09-05 17:29:04.969266	f
3	7	1	2018-09-05 17:30:43.376478	f
4	8	2	2018-09-05 17:31:08.484913	f
5	23	17	2018-09-05 17:31:20.259584	f
6	20	14	2018-09-05 17:31:57.118869	f
7	21	15	2018-09-05 17:32:11.09679	f
8	16	10	2018-09-05 17:32:29.856976	f
9	18	12	2018-09-05 17:33:53.288495	f
10	19	13	2018-09-05 17:34:16.290738	f
11	9	3	2018-09-05 17:34:36.029396	f
12	10	4	2018-09-05 17:34:53.582687	f
13	11	5	2018-09-05 17:35:32.594262	f
14	12	6	2018-09-05 17:35:49.22774	f
15	13	7	2018-09-05 17:36:38.091986	f
16	14	8	2018-09-05 17:36:55.462933	f
17	22	16	2018-09-05 17:37:16.159844	f
18	15	9	2018-09-05 17:38:09.052668	f
19	24	2	2018-09-12 15:08:20.822156	f
20	24	17	2018-09-12 15:08:20.894894	f
21	24	16	2018-09-12 15:08:20.91552	f
22	24	10	2018-09-12 15:08:20.933545	f
23	24	14	2018-09-12 15:08:20.95559	f
24	24	15	2018-09-12 15:08:20.977844	f
25	24	11	2018-09-12 15:08:20.997113	f
26	24	1	2018-09-12 15:08:21.020903	f
27	24	12	2018-09-12 15:08:21.037662	f
28	24	6	2018-09-12 15:08:21.051405	f
29	24	3	2018-09-12 15:08:21.074366	f
30	24	4	2018-09-12 15:08:21.092971	f
31	24	5	2018-09-12 15:08:21.112785	f
32	24	8	2018-09-12 15:08:21.129964	f
33	24	7	2018-09-12 15:08:21.151292	f
34	24	13	2018-09-12 15:08:21.165111	f
35	24	9	2018-09-12 15:08:21.187432	f
\.


--
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.members_id_seq', 35, true);


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.messages (id, board_id, parent_id, subject, content, author_id, replies_count, last_reply_id, created_on, updated_on, locked, sticky) FROM stdin;
\.


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.messages_id_seq', 1, false);


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.news (id, project_id, title, summary, description, author_id, created_on, comments_count) FROM stdin;
\.


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.news_id_seq', 1, false);


--
-- Data for Name: open_id_authentication_associations; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.open_id_authentication_associations (id, issued, lifetime, handle, assoc_type, server_url, secret) FROM stdin;
\.


--
-- Name: open_id_authentication_associations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.open_id_authentication_associations_id_seq', 1, false);


--
-- Data for Name: open_id_authentication_nonces; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.open_id_authentication_nonces (id, "timestamp", server_url, salt) FROM stdin;
\.


--
-- Name: open_id_authentication_nonces_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.open_id_authentication_nonces_id_seq', 1, false);


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.projects (id, name, description, homepage, is_public, parent_id, created_on, updated_on, identifier, status, lft, rgt, inherit_members, default_version_id, default_assigned_to_id) FROM stdin;
3	Parque Arroyo Antequera	Proyecto de parquización que se adapta a la forma natural del arroyo Antequera, prácticamente dentro del cauce, como continuidad del plan de mejoramiento del Barrio Chacharita Alta. El proyecto dotará al área de un equipamiento de uso público, dando valor al relieve natural, la riqueza de la fauna y la flora y del sistema hídrico.	http://asuparticipa.cds.com.py/map?code=parque_arroyo_antequera	f	\N	2018-09-05 14:16:38.7369	2018-09-05 14:16:38.7369	parque_arroyo_antequera	1	19	20	t	\N	\N
4	Parque Caballero	Proyecto de restauración y revitalización del Parque Caballero, uno de los espacios públicos más emblemáticos de Asunción, con alto valor patrimonial e histórico. Se buscará reposicionar al Parque como centro de encuentros sociales y culturales como elemento de unión entre el Río Paraguay y la ciudad de Asunción.	http://asuparticipa.cds.com.py/map?code=parque_caballero	f	\N	2018-09-05 14:18:40.186204	2018-09-05 14:18:40.186204	parque_caballero	1	21	22	t	\N	\N
17	Barrio Cerrito	Encontrando juntos soluciones habitacionales	http://asuparticipa.cds.com.py/map?code=barrio_cerrito	f	\N	2018-09-05 14:39:45.710143	2018-09-05 14:39:45.710143	barrio_cerrito	1	7	8	t	\N	\N
1	Avenida Costanera Sur	Proyecto vial de 8 km de longitud, parte de la Franja Costera Sur, inicia el primer anillo de circunvalación de Asunción y crea un nuevo distrito urbano en el bañado sur. Facilitará el acceso ordenado y fluido al micro-centro de la ciudad desde los alrededores del Cerro Lambaré hasta el Cerro Tacumbú. Este proyecto de infraestructura a ser construido en tierras de la Municipalidad de Asunción será ejecutado por el Ministerio de Obras Públicas y Comunicaciones, como resultado de un pacto político, social, ambiental, económico y financiero acordado entre los moradores del bañado sur y sus organizaciones de base, la Municipalidad de Asunción, el Gobierno Nacional, SENAVITAT e Itaipú Binacional, entre otros actores.	http://asuparticipa.cds.com.py/map?code=costanera_sur	f	\N	2018-08-31 14:12:13.292383	2018-09-05 14:07:09.465566	costanera_sur	1	3	4	t	\N	\N
2	Banco San Miguel	Proyecto de parquización de la reserva con el propósito de proteger y mantener la calidad medioambiental del banco, promover la convivencia social de las comunidades en espacios recreativos, fomentando el sustento productivo de los pescadores y el turismo sustentable en la zona. La infraestructura a ser construida en el área está localizada en la bahía de Asunción - frente mismo al microcentro capitalino. Tendrá un diseño que evite o minimice el impacto ambiental y la presión antrópica sobre el parque.	http://asuparticipa.cds.com.py/map?code=banco_san_miguel	f	\N	2018-09-05 14:14:11.583549	2018-09-05 14:14:11.583549	banco_san_miguel	1	5	6	t	\N	\N
11	Avenida Costanera Norte de Asunción - Segunda Etapa y Conexión Avda. Primer Presidente con Ruta Nacional Nº 9	Transitando entre verdes desde el Palacio de López hasta el Jardín Botánico	http://asuparticipa.cds.com.py/map?code=costanera_norte	f	\N	2018-09-05 14:31:35.95176	2018-09-05 14:31:35.95176	costanera_norte	1	1	2	t	\N	\N
10	Eco-Bahía Centro de Eventos	Proyecto de desarrollo de un nuevo distrito en la ciudad de Asunción, en la Costanera Norte, con espacios públicos de calidad, desarrollos inmobiliarios mixtos, equipamientos recreativos, estructura para espectáculos culturales y deportivos. Incluye la recuperación del histórico arroyo Cará-Cará, para convertirlo en un canal abierto para la práctica del remo. El proyecto utilizará por primera vez la innovadora ordenanza municipal 136/2000 de canje de obras por tierras.	http://asuparticipa.cds.com.py/map?code=centro_eventos	f	\N	2018-09-05 14:29:56.824572	2018-09-05 14:29:56.824572	centro_eventos	1	13	14	t	\N	\N
5	Parque Lineal Costero Norte	Proyecto de parquización de 12 hectáreas, en diseño lineal, de 40 metros de ancho, que sigue el trazado de la Avda. Costanera en toda su longitud, conformando el paseo costero norte hasta el Jardín Botánico. Contendrá una exuberante vegetación con reforestación de la zona rellenada, amplios espacios abiertos, zonas de juegos para niños, circuitos para bicicletas, caminatas, cami-trotes y explanadas para eventos, que complementan a otros espacios públicos preexistentes tales como: el Parque del Bicentenario, el Parque Caballero, el Muelle Deportivo, áreas gastronómicas, puerto revitalizado, entre otros.	http://asuparticipa.cds.com.py/map?code=parque_lineal_costero	f	\N	2018-09-05 14:20:09.358041	2018-09-05 14:20:09.358041	parque_lineal_costero	1	23	24	t	\N	\N
14	Consolidación del Barrio Chacarita Alta	Cuidando la vida costera alta	http://asuparticipa.cds.com.py/map?code=chacarita_alta	f	\N	2018-09-05 14:36:02.522469	2018-09-05 14:36:02.522469	chacarita_alta	1	9	10	t	\N	\N
12	Eco Bahía I	Rescatando la deuda social	http://asuparticipa.cds.com.py/map?code=ecobahia	f	\N	2018-09-05 14:33:21.039942	2018-09-05 14:33:21.039942	ecobahia	1	15	16	t	\N	\N
6	Plaza Mirador del Puerto de Asunción	Proyecto de plaza integrada al plan de Reconversión Urbana del Puerto de Asunción. Se ubicará en medio de edificios históricos y emblemáticos para revitalizar el espacio público preexistente, dándole realce al monumento artístico que ya se encuentra en el lugar. Se pretende ordenar la zona gastronómica, fortaleciendo los empleos, incluso posibilitando que los vendedores de comida de calle, ya instalados de manera desordenada en la zona, continúen trabajando en un ambiente más atractivo y ordenado.	http://asuparticipa.cds.com.py/map?code=mirador_puerto	f	\N	2018-09-05 14:21:25.234301	2018-09-05 14:21:25.234301	mirador_puerto	1	25	26	t	\N	\N
8	Reconversión Urbana del Puerto de Asunción y Terminal del Metrobús	Este subproyecto de revitalización de terrenos y galpones de la zona portuaria para convertirla en un espacio recreativo y gastronómico, con polo comercial, áreas específicas para edificios corporativos privados y nuevos predios destinados a varios Ministerios del Gobierno Nacional, que conformarán un nuevo Centro Cívico de fácil acceso para toda la ciudadanía. \r\n\r\n*Espacios Públicos*\r\nSe incorporará el Puerto de Asunción al sistema de espacios públicos de la ciudad, actualmente explotada para espacios de expresión artística y cultural, el cual será potenciado e impulsado mediante el equipamiento urbano de grandes áreas internas y costeras del predio.\r\n\r\n*Oficinas de Gobierno*\r\nSe realizará la construcción de un nuevo conjunto de edificios que albergarán a Ministerios del Poder Ejecutivo, con la finalidad de crear una nueva centralidad de gestión y trabajo público dentro de un ambiente urbano de alta calidad, y de fácil acceso para la ciudadanía.	http://asuparticipa.cds.com.py/map?code=puerto_asuncion	f	\N	2018-09-05 14:24:57.889426	2018-09-05 14:24:57.889426	puerto_asuncion	1	29	30	t	\N	\N
7	Puerto Deportivo de la Bahía	Proyecto de construcción de muelle, plataformas y otras obras de infraestructura, para equipar la Costanera de Asunción con un programa de uso deportivo-náutico, que contará con accesos directos a la zona de la bahía y con espacios cubiertos para depósitos y guarderías de embarcaciones. Este proyecto se hará en asociación con varias organizaciones que ya realizan actividades deportivas náuticas.	http://asuparticipa.cds.com.py/map?code=puerto_deportivo	f	\N	2018-09-05 14:22:30.932735	2018-09-05 14:22:30.932735	puerto_deportivo	1	27	28	t	\N	\N
13	Nuevo Barrio Tacumbú	Refundando un barrio social	http://asuparticipa.cds.com.py/map?code=tacumbu	f	\N	2018-09-05 14:35:05.674194	2018-09-05 14:35:05.674194	tacumbu	1	17	18	t	\N	\N
15	Consolidación del Barrio Chacarita Baja	Mejorando la vida en los bajos de la costa	http://asuparticipa.cds.com.py/map?code=chacarita_baja	f	\N	2018-09-05 14:37:20.543187	2018-09-05 14:37:20.543187	chacarita_baja	1	11	12	t	\N	\N
9	Villa Olímpica	Proyecto de creación de la Villa de Alojamiento para las delegaciones de atletas participantes de los Juegos Suramericanos 2022(Organización Deportiva Suramericana - ODESUR) en la ciudad de Asunción, elegida como sede para los juegos olímpicos. La Villa posteriormente será destinada a la comercialización en el mercado inmobiliario del segmento medio.	http://asuparticipa.cds.com.py/map?code=villa_olimpica	f	\N	2018-09-05 14:28:48.337508	2018-09-05 14:28:48.337508	villa_olimpica	1	33	34	t	\N	\N
16	Urbanización Cerrito Cará-Cará	Viviendo en un barrio sostenible	http://asuparticipa.cds.com.py/map?code=cara_cara	f	\N	2018-09-05 14:38:19.950149	2018-09-05 14:38:19.950149	cara_cara	1	31	32	t	\N	\N
\.


--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.projects_id_seq', 17, true);


--
-- Data for Name: projects_trackers; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.projects_trackers (project_id, tracker_id) FROM stdin;
1	1
1	2
1	3
2	1
2	2
2	3
3	1
3	2
3	3
4	1
4	2
4	3
5	1
5	2
5	3
6	1
6	2
6	3
7	1
7	2
7	3
8	1
8	2
8	3
9	1
9	2
9	3
10	1
10	2
10	3
11	1
11	2
11	3
12	1
12	2
12	3
13	1
13	2
13	3
14	1
14	2
14	3
15	1
15	2
15	3
16	1
16	2
16	3
17	1
17	2
17	3
\.


--
-- Data for Name: queries; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.queries (id, project_id, name, filters, user_id, column_names, sort_criteria, group_by, type, visibility, options) FROM stdin;
\.


--
-- Name: queries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.queries_id_seq', 1, false);


--
-- Data for Name: queries_roles; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.queries_roles (query_id, role_id) FROM stdin;
\.


--
-- Data for Name: repositories; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.repositories (id, project_id, url, login, password, root_url, type, path_encoding, log_encoding, extra_info, identifier, is_default, created_on) FROM stdin;
\.


--
-- Name: repositories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.repositories_id_seq', 1, false);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.roles (id, name, "position", assignable, builtin, permissions, issues_visibility, users_visibility, time_entries_visibility, all_roles_managed, settings) FROM stdin;
3	Jefe de proyecto	1	t	0	---\n- :add_project\n- :edit_project\n- :close_project\n- :select_project_modules\n- :manage_members\n- :manage_versions\n- :add_subprojects\n- :manage_public_queries\n- :save_queries\n- :view_issues\n- :add_issues\n- :edit_issues\n- :copy_issues\n- :manage_issue_relations\n- :manage_subtasks\n- :set_issues_private\n- :set_own_issues_private\n- :add_issue_notes\n- :edit_issue_notes\n- :edit_own_issue_notes\n- :view_private_notes\n- :set_notes_private\n- :delete_issues\n- :view_issue_watchers\n- :add_issue_watchers\n- :delete_issue_watchers\n- :import_issues\n- :manage_categories\n- :view_time_entries\n- :log_time\n- :edit_time_entries\n- :edit_own_time_entries\n- :manage_project_activities\n- :view_news\n- :manage_news\n- :comment_news\n- :view_documents\n- :add_documents\n- :edit_documents\n- :delete_documents\n- :view_files\n- :manage_files\n- :view_wiki_pages\n- :view_wiki_edits\n- :export_wiki_pages\n- :edit_wiki_pages\n- :rename_wiki_pages\n- :delete_wiki_pages\n- :delete_wiki_pages_attachments\n- :protect_wiki_pages\n- :manage_wiki\n- :view_changesets\n- :browse_repository\n- :commit_access\n- :manage_related_issues\n- :manage_repository\n- :view_messages\n- :add_messages\n- :edit_messages\n- :edit_own_messages\n- :delete_messages\n- :delete_own_messages\n- :manage_boards\n- :view_calendar\n- :view_gantt\n	all	all	all	t	\N
1	Non member	0	t	1	---\n- :view_issues\n- :add_issues\n- :add_issue_notes\n- :save_queries\n- :view_gantt\n- :view_calendar\n- :view_time_entries\n- :view_news\n- :comment_news\n- :view_documents\n- :view_wiki_pages\n- :view_wiki_edits\n- :view_messages\n- :add_messages\n- :view_files\n- :browse_repository\n- :view_changesets\n	default	all	all	t	\N
2	Anonymous	0	t	2	---\n- :view_issues\n- :view_gantt\n- :view_calendar\n- :view_time_entries\n- :view_news\n- :view_documents\n- :view_wiki_pages\n- :view_wiki_edits\n- :view_messages\n- :view_files\n- :browse_repository\n- :view_changesets\n	default	all	all	t	\N
5	Informador	2	t	0	---\n- :view_issues\n- :add_issues\n- :add_issue_notes\n- :save_queries\n- :view_gantt\n- :view_calendar\n- :log_time\n- :view_time_entries\n- :view_news\n- :comment_news\n- :view_documents\n- :view_wiki_pages\n- :view_wiki_edits\n- :view_messages\n- :add_messages\n- :edit_own_messages\n- :view_files\n- :browse_repository\n- :view_changesets\n	default	all	all	t	\N
6	Encargado de proyecto	3	t	0	---\n- :edit_project\n- :close_project\n- :select_project_modules\n- :manage_members\n- :add_subprojects\n- :manage_public_queries\n- :save_queries\n- :view_messages\n- :add_messages\n- :delete_own_messages\n- :view_calendar\n- :view_documents\n- :add_documents\n- :delete_documents\n- :view_files\n- :manage_files\n- :view_gantt\n- :view_issues\n- :add_issues\n- :edit_issues\n- :copy_issues\n- :manage_issue_relations\n- :manage_subtasks\n- :set_issues_private\n- :set_own_issues_private\n- :add_issue_notes\n- :edit_issue_notes\n- :edit_own_issue_notes\n- :view_private_notes\n- :set_notes_private\n- :delete_issues\n- :view_issue_watchers\n- :add_issue_watchers\n- :delete_issue_watchers\n- :import_issues\n- :manage_categories\n- :view_news\n- :comment_news\n- :view_changesets\n- :browse_repository\n- :view_time_entries\n- :view_wiki_pages\n- :view_wiki_edits\n	default	all	all	t	--- !ruby/hash:ActiveSupport::HashWithIndifferentAccess\npermissions_all_trackers: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  view_issues: '1'\n  add_issues: '1'\n  edit_issues: '1'\n  add_issue_notes: '1'\n  delete_issues: '1'\npermissions_tracker_ids: !ruby/hash:ActiveSupport::HashWithIndifferentAccess\n  view_issues: []\n  add_issues: []\n  edit_issues: []\n  add_issue_notes: []\n  delete_issues: []\n
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.roles_id_seq', 6, true);


--
-- Data for Name: roles_managed_roles; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.roles_managed_roles (role_id, managed_role_id) FROM stdin;
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.schema_migrations (version) FROM stdin;
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
93
94
95
96
97
98
99
100
101
102
103
104
105
106
107
108
20090214190337
20090312172426
20090312194159
20090318181151
20090323224724
20090401221305
20090401231134
20090403001910
20090406161854
20090425161243
20090503121501
20090503121505
20090503121510
20090614091200
20090704172350
20090704172355
20090704172358
20091010093521
20091017212227
20091017212457
20091017212644
20091017212938
20091017213027
20091017213113
20091017213151
20091017213228
20091017213257
20091017213332
20091017213444
20091017213536
20091017213642
20091017213716
20091017213757
20091017213835
20091017213910
20091017214015
20091017214107
20091017214136
20091017214236
20091017214308
20091017214336
20091017214406
20091017214440
20091017214519
20091017214611
20091017214644
20091017214720
20091017214750
20091025163651
20091108092559
20091114105931
20091123212029
20091205124427
20091220183509
20091220183727
20091220184736
20091225164732
20091227112908
20100129193402
20100129193813
20100221100219
20100313132032
20100313171051
20100705164950
20100819172912
20101104182107
20101107130441
20101114115114
20101114115359
20110220160626
20110223180944
20110223180953
20110224000000
20110226120112
20110226120132
20110227125750
20110228000000
20110228000100
20110401192910
20110408103312
20110412065600
20110511000000
20110902000000
20111201201315
20120115143024
20120115143100
20120115143126
20120127174243
20120205111326
20120223110929
20120301153455
20120422150750
20120705074331
20120707064544
20120714122000
20120714122100
20120714122200
20120731164049
20120930112914
20121026002032
20121026003537
20121209123234
20121209123358
20121213084931
20130110122628
20130201184705
20130202090625
20130207175206
20130207181455
20130215073721
20130215111127
20130215111141
20130217094251
20130602092539
20130710182539
20130713104233
20130713111657
20130729070143
20130911193200
20131004113137
20131005100610
20131124175346
20131210180802
20131214094309
20131215104612
20131218183023
20140228130325
20140903143914
20140920094058
20141029181752
20141029181824
20141109112308
20141122124142
20150113194759
20150113211532
20150113213922
20150113213955
20150208105930
20150510083747
20150525103953
20150526183158
20150528084820
20150528092912
20150528093249
20150725112753
20150730122707
20150730122735
20150921204850
20150921210243
20151020182334
20151020182731
20151021184614
20151021185456
20151021190616
20151024082034
20151025072118
20151031095005
20160404080304
20160416072926
20160529063352
20161001122012
20161002133421
20161010081301
20161010081528
20161010081600
20161126094932
20161220091118
20170207050700
20170302015225
20170309214320
20170320051650
20170418090031
20170419144536
20170723112801
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.settings (id, name, value, updated_on) FROM stdin;
1	rest_api_enabled	1	2018-08-31 14:15:01.034179
2	jsonp_enabled	1	2018-08-31 14:15:01.048294
3	login_required	1	2018-08-31 14:17:48.301473
4	autologin	0	2018-08-31 14:17:48.311078
5	self_registration	2	2018-08-31 14:17:48.326264
6	show_custom_fields_on_registration	1	2018-08-31 14:17:48.341017
7	password_min_length	8	2018-08-31 14:17:48.34803
8	password_max_age	0	2018-08-31 14:17:48.356885
9	lost_password	1	2018-08-31 14:17:48.364993
10	openid	0	2018-08-31 14:17:48.377193
11	session_lifetime	0	2018-08-31 14:17:48.390887
12	session_timeout	0	2018-08-31 14:17:48.398563
13	mail_from	asuparticipa@gmail.com	2018-09-10 15:00:50.314979
14	bcc_recipients	1	2018-09-10 15:00:50.405629
15	plain_text_mail	0	2018-09-10 15:00:50.419935
16	notified_events	---\n- issue_added\n- issue_updated\n	2018-09-10 15:00:50.428859
17	emails_header		2018-09-10 15:00:50.445232
18	emails_footer	Has recibido este mensaje por que formas parte de un reclamo ciudadano! ve a http://asuparticipa.cds.com.py/reclamos	2018-09-10 15:00:59.705678
19	max_additional_emails	5	2018-09-12 15:41:09.207462
20	unsubscribe	1	2018-09-12 15:41:09.279077
21	default_users_hide_mail	1	2018-09-12 15:41:09.291179
22	default_notification_option	all	2018-09-12 15:41:09.314645
23	default_users_time_zone	La Paz	2018-09-12 15:41:09.331304
24	default_projects_public	1	2018-09-12 15:41:54.189438
25	default_projects_modules	---\n- issue_tracking\n- time_tracking\n- news\n- documents\n- files\n- wiki\n- repository\n- boards\n- calendar\n- gantt\n	2018-09-12 15:41:54.207804
26	default_projects_tracker_ids	---\n- '1'\n	2018-09-12 15:41:54.222161
27	sequential_project_identifiers	0	2018-09-12 15:41:54.234603
\.


--
-- Name: settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.settings_id_seq', 27, true);


--
-- Data for Name: time_entries; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.time_entries (id, project_id, user_id, issue_id, hours, comments, activity_id, spent_on, tyear, tmonth, tweek, created_on, updated_on) FROM stdin;
\.


--
-- Name: time_entries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.time_entries_id_seq', 1, false);


--
-- Data for Name: tokens; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.tokens (id, user_id, action, value, created_on, updated_on) FROM stdin;
3	1	api	0d76118d5af65e582df64639d36b410433285c7f	2018-08-31 14:11:04.832766	2018-08-31 14:11:04.832766
4	1	feeds	d844b223c9dc5e4655ed966da4c786486d25d0ec	2018-08-31 14:11:04.855853	2018-08-31 14:11:04.855853
13	6	session	ab1331ad64e2d1c90f5415447300ea3b6ac156b0	2018-09-03 12:10:25.702288	2018-09-03 15:59:29.682477
32	1	session	e76710913fdac9ecf20f342e25cbdba2df8b47a7	2018-09-12 09:57:54.298541	2018-09-12 16:20:06.626524
30	1	session	fa405a80b0e4ed5736772df97a906dff5530fd00	2018-09-11 08:00:17.120074	2018-09-11 16:02:46.839618
35	1	session	ad2bde204246782e95d989e33a6697e5d97f20ff	2018-09-13 08:03:06.704005	2018-09-13 15:32:54.006983
5	4	api	b9c9cf51766c88ad67f0aa65c6978d9a3071675d	2018-08-31 14:13:22.290363	2018-08-31 14:13:22.290363
36	1	session	52c6726e2f188e93607f0e3fb724dbed53ded20e	2018-09-14 10:36:26.436367	2018-09-14 10:36:26.542795
7	5	api	f95305e50c19911fd9436043922e9bfe96435403	2018-08-31 14:13:27.572027	2018-08-31 14:13:27.572027
8	5	feeds	4e73b6cc8a76978e405c2d9dbfe599ffd69793dc	2018-08-31 14:13:27.591409	2018-08-31 14:13:27.591409
31	1	session	107e914236e97232bafeb6d75b31b52958dd280a	2018-09-12 09:57:20.148263	2018-09-12 15:16:33.965377
34	24	feeds	5d86a748bcd6ab535b3f70c10960419445b95d59	2018-09-12 15:17:12.000978	2018-09-12 15:17:12.000978
33	24	session	1a85ddf4737d05e83fedfbc9631c37a9f3b2475c	2018-09-12 15:17:11.739889	2018-09-12 15:17:18.838411
26	1	session	190a3cf82ace4e922a747126f83d0927eec9cc71	2018-09-10 08:08:49.903807	2018-09-10 17:21:44.727282
18	6	session	b75bee72461873fa4ac0645a52b47a434b5b9d43	2018-09-04 14:11:55.258582	2018-09-04 15:12:13.378252
14	6	api	ee3db964c296393aedaf7c46c69a08da0cae0c83	2018-09-03 12:10:25.855358	2018-09-03 12:10:25.855358
15	6	feeds	baac306d91040d9c47da068c141d48e7c0ebc0aa	2018-09-03 12:10:25.875818	2018-09-03 12:10:25.875818
25	1	session	f77965506ab62e2072e2f28dec5b4ab4b67112c2	2018-09-10 08:03:20.055047	2018-09-10 08:06:37.379361
28	1	session	85d6df9731f3deac6edfefed8aff8cd6446bacb7	2018-09-11 07:53:17.43066	2018-09-11 07:54:23.861873
37	1	session	9efa4636f5949fcab6d49960a06fb6c277215b57	2018-09-14 10:52:32.972121	2018-09-14 17:11:07.405942
29	1	session	a61a888ef6e138f65c478390ee88da93ff280c45	2018-09-11 08:00:07.8153	2018-09-11 08:00:10.31233
\.


--
-- Name: tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.tokens_id_seq', 37, true);


--
-- Data for Name: trackers; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.trackers (id, name, is_in_chlog, "position", is_in_roadmap, fields_bits, default_status_id) FROM stdin;
2	Tareas	t	2	t	0	1
3	Soporte	f	3	f	0	1
1	Reclamo ciudadano	t	1	f	0	1
\.


--
-- Name: trackers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.trackers_id_seq', 3, true);


--
-- Data for Name: user_preferences; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.user_preferences (id, user_id, others, hide_mail, time_zone) FROM stdin;
2	5	---\n:no_self_notified: '1'\n:comments_sorting: asc\n:warn_on_leaving_unsaved: '1'\n:textarea_font: ''\n:my_page_layout:\n  left:\n  - issuesassignedtome\n  right:\n  - issuesreportedbyme\n:my_page_settings: {}\n	t	
3	6	---\n:no_self_notified: '1'\n:comments_sorting: asc\n:warn_on_leaving_unsaved: '1'\n:textarea_font: ''\n:my_page_layout:\n  left:\n  - issuesassignedtome\n  right:\n  - issuesreportedbyme\n:my_page_settings: {}\n	t	Buenos Aires
1	1	---\n:no_self_notified: true\n:my_page_layout:\n  left:\n  - issuesassignedtome\n  right:\n  - issuesreportedbyme\n:my_page_settings: {}\n:gantt_zoom: 2\n:gantt_months: 6\n	t	
4	24	---\n:no_self_notified: '1'\n:comments_sorting: asc\n:warn_on_leaving_unsaved: '1'\n:textarea_font: ''\n:my_page_layout:\n  left:\n  - issuesassignedtome\n  right:\n  - issuesreportedbyme\n:my_page_settings: {}\n	t	La Paz
\.


--
-- Name: user_preferences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.user_preferences_id_seq', 4, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.users (id, login, hashed_password, firstname, lastname, admin, status, last_login_on, language, auth_source_id, created_on, updated_on, type, identity_url, mail_notification, salt, must_change_passwd, passwd_changed_on) FROM stdin;
2				Anonymous users	f	1	\N		\N	2018-08-31 14:10:03.836299	2018-08-31 14:10:03.836299	GroupAnonymous	\N		\N	f	\N
3				Non member users	f	1	\N		\N	2018-08-31 14:10:03.873737	2018-08-31 14:10:03.873737	GroupNonMember	\N		\N	f	\N
4				Anonymous	f	0	\N		\N	2018-08-31 14:10:48.761521	2018-08-31 14:10:48.761521	AnonymousUser	\N	only_my_events	\N	f	\N
1	admin	be7fecf172353d9f9f8bf21a47c95ab26242fa42	Redmine	Admin	t	1	2018-09-14 10:52:32.922265		\N	2018-08-31 14:09:43.727441	2018-08-31 14:11:01.024098	User	\N	all	72aeab3d075988fc805b876f3abb98fb	f	2018-08-31 14:11:01
5	sinpermisos	feed3d605b9acc69b839e1f3808d72451a9a75c3	sinpermisos	sinpermisos	f	1	2018-08-31 14:13:27.412669	en	\N	2018-08-31 14:13:18.518829	2018-08-31 14:13:18.518829	User	\N	only_my_events	1dd45ce3612a61b4a42e4ea7a1754b20	f	2018-08-31 14:13:18
6	express	1f51aca397fdd36565b2108813611489b89c555c	Usuario para expres	De prueba	f	1	2018-09-05 09:04:56.513612	es	\N	2018-09-03 11:43:11.003691	2018-09-03 11:43:11.003691	User	\N	only_my_events	2de9e6972c59f97bb213733daa735952	f	2018-09-03 11:43:11
7				costanera_sur	f	1	\N		\N	2018-09-05 17:15:07.792438	2018-09-05 17:15:07.792438	Group	\N		\N	f	\N
8				banco_san_miguel	f	1	\N		\N	2018-09-05 17:15:18.251576	2018-09-05 17:15:18.251576	Group	\N		\N	f	\N
9				parque_arroyo_antequera	f	1	\N		\N	2018-09-05 17:15:25.875135	2018-09-05 17:15:25.875135	Group	\N		\N	f	\N
10				parque_caballero	f	1	\N		\N	2018-09-05 17:15:31.049859	2018-09-05 17:15:31.049859	Group	\N		\N	f	\N
11				parque_lineal_costero	f	1	\N		\N	2018-09-05 17:15:37.025763	2018-09-05 17:15:37.025763	Group	\N		\N	f	\N
12				mirador_puerto	f	1	\N		\N	2018-09-05 17:15:42.565868	2018-09-05 17:15:42.565868	Group	\N		\N	f	\N
13				puerto_deportivo	f	1	\N		\N	2018-09-05 17:15:47.706218	2018-09-05 17:15:47.706218	Group	\N		\N	f	\N
14				puerto_asuncion	f	1	\N		\N	2018-09-05 17:15:52.757407	2018-09-05 17:15:52.757407	Group	\N		\N	f	\N
15				villa_olimpica	f	1	\N		\N	2018-09-05 17:15:58.680184	2018-09-05 17:15:58.680184	Group	\N		\N	f	\N
16				centro_eventos	f	1	\N		\N	2018-09-05 17:16:05.014127	2018-09-05 17:16:05.014127	Group	\N		\N	f	\N
17				costanera_norte	f	1	\N		\N	2018-09-05 17:16:11.363873	2018-09-05 17:16:11.363873	Group	\N		\N	f	\N
18				ecobahia	f	1	\N		\N	2018-09-05 17:16:15.54882	2018-09-05 17:16:15.54882	Group	\N		\N	f	\N
19				tacumbu	f	1	\N		\N	2018-09-05 17:16:22.404624	2018-09-05 17:16:22.404624	Group	\N		\N	f	\N
20				chacarita_alta	f	1	\N		\N	2018-09-05 17:16:28.810064	2018-09-05 17:16:28.810064	Group	\N		\N	f	\N
21				chacarita_baja	f	1	\N		\N	2018-09-05 17:16:33.935583	2018-09-05 17:16:33.935583	Group	\N		\N	f	\N
22				cara_cara	f	1	\N		\N	2018-09-05 17:16:38.378453	2018-09-05 17:16:38.378453	Group	\N		\N	f	\N
23				barrio_cerrito	f	1	\N		\N	2018-09-05 17:16:42.993504	2018-09-05 17:16:42.993504	Group	\N		\N	f	\N
24	prueba1	eec39786b198d7a26f3dd810a75522d54007393c	Prueba	Numero1	f	1	2018-09-12 15:17:11.712768	es	\N	2018-09-12 15:08:01.583034	2018-09-12 15:08:01.583034	User	\N	only_my_events	7449c2acf4c542e168c4dbacf22d829f	f	2018-09-12 15:08:01
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.users_id_seq', 24, true);


--
-- Data for Name: versions; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.versions (id, project_id, name, description, effective_date, created_on, updated_on, wiki_page_title, status, sharing) FROM stdin;
\.


--
-- Name: versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.versions_id_seq', 1, false);


--
-- Data for Name: watchers; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.watchers (id, watchable_type, watchable_id, user_id) FROM stdin;
\.


--
-- Name: watchers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.watchers_id_seq', 1, false);


--
-- Data for Name: wiki_content_versions; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.wiki_content_versions (id, wiki_content_id, page_id, author_id, data, compression, comments, updated_on, version) FROM stdin;
\.


--
-- Name: wiki_content_versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.wiki_content_versions_id_seq', 1, false);


--
-- Data for Name: wiki_contents; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.wiki_contents (id, page_id, author_id, text, comments, updated_on, version) FROM stdin;
\.


--
-- Name: wiki_contents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.wiki_contents_id_seq', 1, false);


--
-- Data for Name: wiki_pages; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.wiki_pages (id, wiki_id, title, created_on, protected, parent_id) FROM stdin;
\.


--
-- Name: wiki_pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.wiki_pages_id_seq', 1, false);


--
-- Data for Name: wiki_redirects; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.wiki_redirects (id, wiki_id, title, redirects_to, created_on, redirects_to_wiki_id) FROM stdin;
\.


--
-- Name: wiki_redirects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.wiki_redirects_id_seq', 1, false);


--
-- Data for Name: wikis; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.wikis (id, project_id, start_page, status) FROM stdin;
1	1	Wiki	1
\.


--
-- Name: wikis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.wikis_id_seq', 1, true);


--
-- Data for Name: workflows; Type: TABLE DATA; Schema: public; Owner: redmine
--

COPY public.workflows (id, tracker_id, old_status_id, new_status_id, role_id, assignee, author, type, field_name, rule) FROM stdin;
1	1	1	2	3	f	f	WorkflowTransition	\N	\N
2	1	1	3	3	f	f	WorkflowTransition	\N	\N
5	1	1	6	3	f	f	WorkflowTransition	\N	\N
6	1	2	1	3	f	f	WorkflowTransition	\N	\N
7	1	2	3	3	f	f	WorkflowTransition	\N	\N
10	1	2	6	3	f	f	WorkflowTransition	\N	\N
11	1	3	1	3	f	f	WorkflowTransition	\N	\N
12	1	3	2	3	f	f	WorkflowTransition	\N	\N
15	1	3	6	3	f	f	WorkflowTransition	\N	\N
26	1	6	1	3	f	f	WorkflowTransition	\N	\N
27	1	6	2	3	f	f	WorkflowTransition	\N	\N
28	1	6	3	3	f	f	WorkflowTransition	\N	\N
31	2	1	2	3	f	f	WorkflowTransition	\N	\N
32	2	1	3	3	f	f	WorkflowTransition	\N	\N
35	2	1	6	3	f	f	WorkflowTransition	\N	\N
36	2	2	1	3	f	f	WorkflowTransition	\N	\N
37	2	2	3	3	f	f	WorkflowTransition	\N	\N
40	2	2	6	3	f	f	WorkflowTransition	\N	\N
41	2	3	1	3	f	f	WorkflowTransition	\N	\N
42	2	3	2	3	f	f	WorkflowTransition	\N	\N
45	2	3	6	3	f	f	WorkflowTransition	\N	\N
56	2	6	1	3	f	f	WorkflowTransition	\N	\N
57	2	6	2	3	f	f	WorkflowTransition	\N	\N
58	2	6	3	3	f	f	WorkflowTransition	\N	\N
61	3	1	2	3	f	f	WorkflowTransition	\N	\N
62	3	1	3	3	f	f	WorkflowTransition	\N	\N
65	3	1	6	3	f	f	WorkflowTransition	\N	\N
66	3	2	1	3	f	f	WorkflowTransition	\N	\N
67	3	2	3	3	f	f	WorkflowTransition	\N	\N
70	3	2	6	3	f	f	WorkflowTransition	\N	\N
71	3	3	1	3	f	f	WorkflowTransition	\N	\N
72	3	3	2	3	f	f	WorkflowTransition	\N	\N
75	3	3	6	3	f	f	WorkflowTransition	\N	\N
86	3	6	1	3	f	f	WorkflowTransition	\N	\N
87	3	6	2	3	f	f	WorkflowTransition	\N	\N
88	3	6	3	3	f	f	WorkflowTransition	\N	\N
\.


--
-- Name: workflows_id_seq; Type: SEQUENCE SET; Schema: public; Owner: redmine
--

SELECT pg_catalog.setval('public.workflows_id_seq', 144, true);


--
-- Name: ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: auth_sources_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.auth_sources
    ADD CONSTRAINT auth_sources_pkey PRIMARY KEY (id);


--
-- Name: boards_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.boards
    ADD CONSTRAINT boards_pkey PRIMARY KEY (id);


--
-- Name: changes_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_pkey PRIMARY KEY (id);


--
-- Name: changesets_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.changesets
    ADD CONSTRAINT changesets_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: custom_field_enumerations_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.custom_field_enumerations
    ADD CONSTRAINT custom_field_enumerations_pkey PRIMARY KEY (id);


--
-- Name: custom_fields_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.custom_fields
    ADD CONSTRAINT custom_fields_pkey PRIMARY KEY (id);


--
-- Name: custom_values_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.custom_values
    ADD CONSTRAINT custom_values_pkey PRIMARY KEY (id);


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: email_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.email_addresses
    ADD CONSTRAINT email_addresses_pkey PRIMARY KEY (id);


--
-- Name: enabled_modules_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.enabled_modules
    ADD CONSTRAINT enabled_modules_pkey PRIMARY KEY (id);


--
-- Name: enumerations_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.enumerations
    ADD CONSTRAINT enumerations_pkey PRIMARY KEY (id);


--
-- Name: import_items_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.import_items
    ADD CONSTRAINT import_items_pkey PRIMARY KEY (id);


--
-- Name: imports_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.imports
    ADD CONSTRAINT imports_pkey PRIMARY KEY (id);


--
-- Name: issue_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issue_categories
    ADD CONSTRAINT issue_categories_pkey PRIMARY KEY (id);


--
-- Name: issue_relations_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issue_relations
    ADD CONSTRAINT issue_relations_pkey PRIMARY KEY (id);


--
-- Name: issue_statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issue_statuses
    ADD CONSTRAINT issue_statuses_pkey PRIMARY KEY (id);


--
-- Name: issues_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);


--
-- Name: journal_details_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.journal_details
    ADD CONSTRAINT journal_details_pkey PRIMARY KEY (id);


--
-- Name: journals_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.journals
    ADD CONSTRAINT journals_pkey PRIMARY KEY (id);


--
-- Name: member_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.member_roles
    ADD CONSTRAINT member_roles_pkey PRIMARY KEY (id);


--
-- Name: members_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_pkey PRIMARY KEY (id);


--
-- Name: messages_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: news_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: open_id_authentication_associations_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.open_id_authentication_associations
    ADD CONSTRAINT open_id_authentication_associations_pkey PRIMARY KEY (id);


--
-- Name: open_id_authentication_nonces_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.open_id_authentication_nonces
    ADD CONSTRAINT open_id_authentication_nonces_pkey PRIMARY KEY (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: queries_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.queries
    ADD CONSTRAINT queries_pkey PRIMARY KEY (id);


--
-- Name: repositories_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.repositories
    ADD CONSTRAINT repositories_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: time_entries_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.time_entries
    ADD CONSTRAINT time_entries_pkey PRIMARY KEY (id);


--
-- Name: tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: trackers_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.trackers
    ADD CONSTRAINT trackers_pkey PRIMARY KEY (id);


--
-- Name: user_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.user_preferences
    ADD CONSTRAINT user_preferences_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: versions_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.versions
    ADD CONSTRAINT versions_pkey PRIMARY KEY (id);


--
-- Name: watchers_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.watchers
    ADD CONSTRAINT watchers_pkey PRIMARY KEY (id);


--
-- Name: wiki_content_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_content_versions
    ADD CONSTRAINT wiki_content_versions_pkey PRIMARY KEY (id);


--
-- Name: wiki_contents_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_contents
    ADD CONSTRAINT wiki_contents_pkey PRIMARY KEY (id);


--
-- Name: wiki_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_pages
    ADD CONSTRAINT wiki_pages_pkey PRIMARY KEY (id);


--
-- Name: wiki_redirects_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wiki_redirects
    ADD CONSTRAINT wiki_redirects_pkey PRIMARY KEY (id);


--
-- Name: wikis_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.wikis
    ADD CONSTRAINT wikis_pkey PRIMARY KEY (id);


--
-- Name: workflows_pkey; Type: CONSTRAINT; Schema: public; Owner: redmine
--

ALTER TABLE ONLY public.workflows
    ADD CONSTRAINT workflows_pkey PRIMARY KEY (id);


--
-- Name: boards_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX boards_project_id ON public.boards USING btree (project_id);


--
-- Name: changeset_parents_changeset_ids; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX changeset_parents_changeset_ids ON public.changeset_parents USING btree (changeset_id);


--
-- Name: changeset_parents_parent_ids; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX changeset_parents_parent_ids ON public.changeset_parents USING btree (parent_id);


--
-- Name: changesets_changeset_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX changesets_changeset_id ON public.changes USING btree (changeset_id);


--
-- Name: changesets_issues_ids; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX changesets_issues_ids ON public.changesets_issues USING btree (changeset_id, issue_id);


--
-- Name: changesets_repos_rev; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX changesets_repos_rev ON public.changesets USING btree (repository_id, revision);


--
-- Name: changesets_repos_scmid; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX changesets_repos_scmid ON public.changesets USING btree (repository_id, scmid);


--
-- Name: custom_fields_roles_ids; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX custom_fields_roles_ids ON public.custom_fields_roles USING btree (custom_field_id, role_id);


--
-- Name: custom_values_customized; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX custom_values_customized ON public.custom_values USING btree (customized_type, customized_id);


--
-- Name: documents_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX documents_project_id ON public.documents USING btree (project_id);


--
-- Name: enabled_modules_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX enabled_modules_project_id ON public.enabled_modules USING btree (project_id);


--
-- Name: groups_users_ids; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX groups_users_ids ON public.groups_users USING btree (group_id, user_id);


--
-- Name: index_attachments_on_author_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_attachments_on_author_id ON public.attachments USING btree (author_id);


--
-- Name: index_attachments_on_container_id_and_container_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_attachments_on_container_id_and_container_type ON public.attachments USING btree (container_id, container_type);


--
-- Name: index_attachments_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_attachments_on_created_on ON public.attachments USING btree (created_on);


--
-- Name: index_attachments_on_disk_filename; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_attachments_on_disk_filename ON public.attachments USING btree (disk_filename);


--
-- Name: index_auth_sources_on_id_and_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_auth_sources_on_id_and_type ON public.auth_sources USING btree (id, type);


--
-- Name: index_boards_on_last_message_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_boards_on_last_message_id ON public.boards USING btree (last_message_id);


--
-- Name: index_changesets_issues_on_issue_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_changesets_issues_on_issue_id ON public.changesets_issues USING btree (issue_id);


--
-- Name: index_changesets_on_committed_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_changesets_on_committed_on ON public.changesets USING btree (committed_on);


--
-- Name: index_changesets_on_repository_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_changesets_on_repository_id ON public.changesets USING btree (repository_id);


--
-- Name: index_changesets_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_changesets_on_user_id ON public.changesets USING btree (user_id);


--
-- Name: index_comments_on_author_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_comments_on_author_id ON public.comments USING btree (author_id);


--
-- Name: index_comments_on_commented_id_and_commented_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_comments_on_commented_id_and_commented_type ON public.comments USING btree (commented_id, commented_type);


--
-- Name: index_custom_fields_on_id_and_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_custom_fields_on_id_and_type ON public.custom_fields USING btree (id, type);


--
-- Name: index_custom_fields_projects_on_custom_field_id_and_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX index_custom_fields_projects_on_custom_field_id_and_project_id ON public.custom_fields_projects USING btree (custom_field_id, project_id);


--
-- Name: index_custom_fields_trackers_on_custom_field_id_and_tracker_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX index_custom_fields_trackers_on_custom_field_id_and_tracker_id ON public.custom_fields_trackers USING btree (custom_field_id, tracker_id);


--
-- Name: index_custom_values_on_custom_field_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_custom_values_on_custom_field_id ON public.custom_values USING btree (custom_field_id);


--
-- Name: index_documents_on_category_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_documents_on_category_id ON public.documents USING btree (category_id);


--
-- Name: index_documents_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_documents_on_created_on ON public.documents USING btree (created_on);


--
-- Name: index_email_addresses_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_email_addresses_on_user_id ON public.email_addresses USING btree (user_id);


--
-- Name: index_enumerations_on_id_and_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_enumerations_on_id_and_type ON public.enumerations USING btree (id, type);


--
-- Name: index_enumerations_on_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_enumerations_on_project_id ON public.enumerations USING btree (project_id);


--
-- Name: index_issue_categories_on_assigned_to_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issue_categories_on_assigned_to_id ON public.issue_categories USING btree (assigned_to_id);


--
-- Name: index_issue_relations_on_issue_from_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issue_relations_on_issue_from_id ON public.issue_relations USING btree (issue_from_id);


--
-- Name: index_issue_relations_on_issue_from_id_and_issue_to_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX index_issue_relations_on_issue_from_id_and_issue_to_id ON public.issue_relations USING btree (issue_from_id, issue_to_id);


--
-- Name: index_issue_relations_on_issue_to_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issue_relations_on_issue_to_id ON public.issue_relations USING btree (issue_to_id);


--
-- Name: index_issue_statuses_on_is_closed; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issue_statuses_on_is_closed ON public.issue_statuses USING btree (is_closed);


--
-- Name: index_issue_statuses_on_position; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issue_statuses_on_position ON public.issue_statuses USING btree ("position");


--
-- Name: index_issues_on_assigned_to_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_assigned_to_id ON public.issues USING btree (assigned_to_id);


--
-- Name: index_issues_on_author_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_author_id ON public.issues USING btree (author_id);


--
-- Name: index_issues_on_category_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_category_id ON public.issues USING btree (category_id);


--
-- Name: index_issues_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_created_on ON public.issues USING btree (created_on);


--
-- Name: index_issues_on_fixed_version_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_fixed_version_id ON public.issues USING btree (fixed_version_id);


--
-- Name: index_issues_on_parent_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_parent_id ON public.issues USING btree (parent_id);


--
-- Name: index_issues_on_priority_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_priority_id ON public.issues USING btree (priority_id);


--
-- Name: index_issues_on_root_id_and_lft_and_rgt; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_root_id_and_lft_and_rgt ON public.issues USING btree (root_id, lft, rgt);


--
-- Name: index_issues_on_status_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_status_id ON public.issues USING btree (status_id);


--
-- Name: index_issues_on_tracker_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_issues_on_tracker_id ON public.issues USING btree (tracker_id);


--
-- Name: index_journals_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_journals_on_created_on ON public.journals USING btree (created_on);


--
-- Name: index_journals_on_journalized_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_journals_on_journalized_id ON public.journals USING btree (journalized_id);


--
-- Name: index_journals_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_journals_on_user_id ON public.journals USING btree (user_id);


--
-- Name: index_member_roles_on_inherited_from; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_member_roles_on_inherited_from ON public.member_roles USING btree (inherited_from);


--
-- Name: index_member_roles_on_member_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_member_roles_on_member_id ON public.member_roles USING btree (member_id);


--
-- Name: index_member_roles_on_role_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_member_roles_on_role_id ON public.member_roles USING btree (role_id);


--
-- Name: index_members_on_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_members_on_project_id ON public.members USING btree (project_id);


--
-- Name: index_members_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_members_on_user_id ON public.members USING btree (user_id);


--
-- Name: index_members_on_user_id_and_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX index_members_on_user_id_and_project_id ON public.members USING btree (user_id, project_id);


--
-- Name: index_messages_on_author_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_messages_on_author_id ON public.messages USING btree (author_id);


--
-- Name: index_messages_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_messages_on_created_on ON public.messages USING btree (created_on);


--
-- Name: index_messages_on_last_reply_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_messages_on_last_reply_id ON public.messages USING btree (last_reply_id);


--
-- Name: index_news_on_author_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_news_on_author_id ON public.news USING btree (author_id);


--
-- Name: index_news_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_news_on_created_on ON public.news USING btree (created_on);


--
-- Name: index_projects_on_lft; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_projects_on_lft ON public.projects USING btree (lft);


--
-- Name: index_projects_on_rgt; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_projects_on_rgt ON public.projects USING btree (rgt);


--
-- Name: index_queries_on_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_queries_on_project_id ON public.queries USING btree (project_id);


--
-- Name: index_queries_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_queries_on_user_id ON public.queries USING btree (user_id);


--
-- Name: index_repositories_on_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_repositories_on_project_id ON public.repositories USING btree (project_id);


--
-- Name: index_roles_managed_roles_on_role_id_and_managed_role_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX index_roles_managed_roles_on_role_id_and_managed_role_id ON public.roles_managed_roles USING btree (role_id, managed_role_id);


--
-- Name: index_settings_on_name; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_settings_on_name ON public.settings USING btree (name);


--
-- Name: index_time_entries_on_activity_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_time_entries_on_activity_id ON public.time_entries USING btree (activity_id);


--
-- Name: index_time_entries_on_created_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_time_entries_on_created_on ON public.time_entries USING btree (created_on);


--
-- Name: index_time_entries_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_time_entries_on_user_id ON public.time_entries USING btree (user_id);


--
-- Name: index_tokens_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_tokens_on_user_id ON public.tokens USING btree (user_id);


--
-- Name: index_user_preferences_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_user_preferences_on_user_id ON public.user_preferences USING btree (user_id);


--
-- Name: index_users_on_auth_source_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_users_on_auth_source_id ON public.users USING btree (auth_source_id);


--
-- Name: index_users_on_id_and_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_users_on_id_and_type ON public.users USING btree (id, type);


--
-- Name: index_users_on_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_users_on_type ON public.users USING btree (type);


--
-- Name: index_versions_on_sharing; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_versions_on_sharing ON public.versions USING btree (sharing);


--
-- Name: index_watchers_on_user_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_watchers_on_user_id ON public.watchers USING btree (user_id);


--
-- Name: index_watchers_on_watchable_id_and_watchable_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_watchers_on_watchable_id_and_watchable_type ON public.watchers USING btree (watchable_id, watchable_type);


--
-- Name: index_wiki_content_versions_on_updated_on; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_wiki_content_versions_on_updated_on ON public.wiki_content_versions USING btree (updated_on);


--
-- Name: index_wiki_contents_on_author_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_wiki_contents_on_author_id ON public.wiki_contents USING btree (author_id);


--
-- Name: index_wiki_pages_on_parent_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_wiki_pages_on_parent_id ON public.wiki_pages USING btree (parent_id);


--
-- Name: index_wiki_pages_on_wiki_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_wiki_pages_on_wiki_id ON public.wiki_pages USING btree (wiki_id);


--
-- Name: index_wiki_redirects_on_wiki_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_wiki_redirects_on_wiki_id ON public.wiki_redirects USING btree (wiki_id);


--
-- Name: index_workflows_on_new_status_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_workflows_on_new_status_id ON public.workflows USING btree (new_status_id);


--
-- Name: index_workflows_on_old_status_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_workflows_on_old_status_id ON public.workflows USING btree (old_status_id);


--
-- Name: index_workflows_on_role_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_workflows_on_role_id ON public.workflows USING btree (role_id);


--
-- Name: index_workflows_on_tracker_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX index_workflows_on_tracker_id ON public.workflows USING btree (tracker_id);


--
-- Name: issue_categories_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX issue_categories_project_id ON public.issue_categories USING btree (project_id);


--
-- Name: issues_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX issues_project_id ON public.issues USING btree (project_id);


--
-- Name: journal_details_journal_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX journal_details_journal_id ON public.journal_details USING btree (journal_id);


--
-- Name: journals_journalized_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX journals_journalized_id ON public.journals USING btree (journalized_id, journalized_type);


--
-- Name: messages_board_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX messages_board_id ON public.messages USING btree (board_id);


--
-- Name: messages_parent_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX messages_parent_id ON public.messages USING btree (parent_id);


--
-- Name: news_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX news_project_id ON public.news USING btree (project_id);


--
-- Name: projects_trackers_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX projects_trackers_project_id ON public.projects_trackers USING btree (project_id);


--
-- Name: projects_trackers_unique; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX projects_trackers_unique ON public.projects_trackers USING btree (project_id, tracker_id);


--
-- Name: queries_roles_ids; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX queries_roles_ids ON public.queries_roles USING btree (query_id, role_id);


--
-- Name: time_entries_issue_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX time_entries_issue_id ON public.time_entries USING btree (issue_id);


--
-- Name: time_entries_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX time_entries_project_id ON public.time_entries USING btree (project_id);


--
-- Name: tokens_value; Type: INDEX; Schema: public; Owner: redmine
--

CREATE UNIQUE INDEX tokens_value ON public.tokens USING btree (value);


--
-- Name: versions_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX versions_project_id ON public.versions USING btree (project_id);


--
-- Name: watchers_user_id_type; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX watchers_user_id_type ON public.watchers USING btree (user_id, watchable_type);


--
-- Name: wiki_content_versions_wcid; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX wiki_content_versions_wcid ON public.wiki_content_versions USING btree (wiki_content_id);


--
-- Name: wiki_contents_page_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX wiki_contents_page_id ON public.wiki_contents USING btree (page_id);


--
-- Name: wiki_pages_wiki_id_title; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX wiki_pages_wiki_id_title ON public.wiki_pages USING btree (wiki_id, title);


--
-- Name: wiki_redirects_wiki_id_title; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX wiki_redirects_wiki_id_title ON public.wiki_redirects USING btree (wiki_id, title);


--
-- Name: wikis_project_id; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX wikis_project_id ON public.wikis USING btree (project_id);


--
-- Name: wkfs_role_tracker_old_status; Type: INDEX; Schema: public; Owner: redmine
--

CREATE INDEX wkfs_role_tracker_old_status ON public.workflows USING btree (role_id, tracker_id, old_status_id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: redmine
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM redmine;
GRANT ALL ON SCHEMA public TO redmine;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

