require 'securerandom'

class EmailIssueHookListener < Redmine::Hook::ViewListener

  @@PARTICIPA_SEGUIMIENTO_URL = "#{ENV['FRONTEND_BASE_URL']}seguimiento/reclamos/"
  @@FROM_EMAIL = ENV['FROM_EMAIL']

  @@SEPARATOR_COLORS = [ '#ffd20a', '#da7127', '#0ad2ff', '#4d3b89', '#00a64f', '#ffd20a', '#da7127', '#0ad2ff', '#4d3b89', '#00a64f' ]
  @@FACEBOOK_BASE64 = ""
  @@TWITTER_BASE64 = ""
  @@INSTAGRAM_BASE64 = ""

  def controller_issues_new_after_save(context = {})
    issue = context[:issue]
    project = Project.find(issue.project_id)
    smtp_settings = context[:config].email_delivery[:smtp_settings]
    user = self.get_user_name_and_email(context[:params][:issue], smtp_settings)
    uuid = SecureRandom.hex(4)
    issue.custom_field_values = {'6' => uuid}
    issue.save
    content = ""\
      "#{self.get_separator_div_color()}"\
      "<h1>Proyecto: '#{project.name}'</h1>"\
      "#{self.get_separator_div_color()}"\
      "<p>Su mensaje ha sido creado con el siguiente identificador #{uuid},\nni bien sea procesado se le avisará por este medio.</p>"\
      "<p>Para hacer seguimiento de su mensaje, haga click <a href='#{self.get_redirect_url(smtp_settings)}#{project.identifier}/#{uuid}' >aquí.</a></p>"
    ActionMailer::Base.mail(
      from: self.get_from_email(smtp_settings),
      to: user[:email],
      content_type: "text/html",
      subject: "[ASUPARTICIPA] Su mensaje con identificador [#{uuid}] ha sido enviado",
      body: self.get_html_body(content)
    ).deliver
  end

  def controller_issues_edit_after_save(context = {})
    issue = context[:issue]
    if issue.status_id == 3 or issue.status_id == 6 then
      project = context[:project]
      journal = context[:journal]
      smtp_settings = context[:config].email_delivery[:smtp_settings]
      notes = journal.notes.present? ? journal.notes : "Su mensaje ha sido procesado pero no tiene respuesta"
      user = self.get_user_name_and_email(context[:params][:issue], smtp_settings)
      uuid = issue.custom_field_value 6 #get custom_field by id = 6
      if issue.status_id == 3
        status = "Resuelto"
      else
        status = "Cerrado"
      end
      subject = "[ASUPARTICIPA] Su mensaje con identificador [#{uuid}] ha sido #{status}"
      content = ""\
        "#{self.get_separator_div_color()}"\
        "<h1>Mensaje: '#{project.name}'</h1>"\
        "#{self.get_separator_div_color()}"\
        "<p>El mensaje perteneciente al proyecto '#{project.name}' con el identificador: #{uuid} y descripción <br/>#{issue.description}</p>"\
        "<p>La respuesta a su mensaje es la siguiente: \n\n#{notes}</p>"
      ActionMailer::Base.mail(
        from: self.get_from_email(smtp_settings),
        to: user[:email],
        content_type: "text/html",
        subject: subject,
        body: self.get_html_body(content)
      ).deliver
    end
  end

  def get_user_name_and_email(issue, smtp_settings)
    userEmail = self.get_from_email(smtp_settings)
    userName = "Usuario solicitante"
    fields = issue[:custom_fields] || issue[:custom_field_values]
    if (fields != nil) then
      fields.each do |x|
        if x.kind_of?(Array)
          if x[0] == "4"
            userName = x[1]
          elsif x[0] == "5"
            userEmail = x[1]
          end
        elsif x[:id] == 4
        elsif x[:id] == 4
          userName = x[:value]
        elsif x[:id] == 5
          userEmail = x[:value]
        end
      end
    end
    return { :name => userName, :email => userEmail }
  end

  def get_redirect_url (smtp_settings)
    return @@PARTICIPA_SEGUIMIENTO_URL
  end

  def get_from_email (smtp_settings)
    return @@FROM_EMAIL
  end

  def get_html_body (content)
    body = ""\
    "<!DOCTYPE html>"\
    "<html>"\
      "<head>"\
        "<meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />"\
        "<style>"\
          "h1 {"\
            "padding: 0 20px;"\
          "}"\
          "p {"\
            "font-size: 16px;"\
            "padding: 0 20px;"\
          "}"\
          ".main {"\
            "background-color: #ccc;"\
            "padding: 30px 0;"\
          "}"\
          ".content {"\
            "background-color: white;"\
            "margin: 0 50px;"\
            "padding-bottom: 20px;"\
          "}"\
          ".header-separator {"\
            "display: block;"\
            "line-height: 0;"\
            "width: 100%;"\
          "}"\
          ".separator {"\
            "display: inline-block;"\
            "width: calc(100% / 10);"\
            "height: 4px;"\
          "}"\
          ".social-media-container {"\
            "display: flex;"\
            "align-items: center;"\
            "justify-content: center;"\
            "width: 100%;"\
          "}"\
          ".social-media {"\
            "background-color: #00aeef;"\
            "border-radius: 50%;"\
            "color: white;"\
            "display: inline-block;"\
            "justify-content: center;"\
            "align-items: center;"\
            "height: 40px;"\
            "width: 40px;"\
          "}"\
          ".social-media.facebook span {"\
            "font-size: 16px;"\
            "font-weight: bold;"\
          "}"\
          ".social-media.twitter span {"\
            "font-size: 16px;"\
            "font-weight: bold;"\
          "}"\
          ".social-media.instagram span {"\
            "font-size: 16px;"\
            "font-weight: bold;"\
          "}"\
        "</style>"\
      "</head>"\
      "<body>"\
        "<div class='main'>"\
          "<div class='content'>"\
            "#{content}"\
            # "<div class=\"social-media-container\">"\
            #   "<a class=\"social-media facebook\" href=\"\">"\
            #     "<span>f</span>"\
            #     # "<img src=\"data:image/jpg;base64,#{@@FACEBOOK_BASE64}\" alt=\"twitter\" />"\
            #   "</a>"\
            #   "<a class=\"social-media twitter\" href=\"\">"\
            #     "<span>t</span>"\
            #     # "<img src=\"data:image/jpg;base64,#{@@TWITTER_BASE64}\" alt=\"twitter\" />"\
            #   "</a>"\
            #   "<a class=\"social-media instagram\" href=\"\">"\
            #     "<span>i</span>"\
            #     # "<img src=\"data:image/jpg;base64,#{@@INSTAGRAM_BASE64}\" alt=\"twitter\" />"\
            #   "</a>"\
            # "</div>"\
          "</div>"\
        "</div>"\
      "</body>"\
    "</html>"
    return body
  end

  def get_separator_div_color()
    separator = "<div class='header-separator'>"
    @@SEPARATOR_COLORS.each do |color|
      separator = separator + "<div class='separator' style='background-color: " + color + ";'></div>"
    end
    separator = separator + "</div>"
    return separator
  end

end
