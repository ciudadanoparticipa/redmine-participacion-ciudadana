#!/bin/bash

# rake db:create
# rake db:migrate

FILE_TO_DELETE="$PWD/tmp/pids/server.pid"

if [ -f "$FILE_TO_DELETE" ]; then rm -rf "$FILE_TO_DELETE"; fi

bundle exec rails server -b 0.0.0.0 -p 3000
