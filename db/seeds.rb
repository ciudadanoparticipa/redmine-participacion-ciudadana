# ruby encoding: utf-8

# setting a few options in DB

presettings = [
		{ name: 'app_title', value: 'Gestión de Participación Ciudadana'},
		{ name: 'ui_theme', value: 'circle'}
]

presettings.each{ | presetting |
	setting = Setting.find_by(name: presetting[:name])
	if setting == nil
		Setting.create!(name: presetting[:name], value: presetting[:value])
	else
		if setting.value != presetting[:value]
			setting.value = presetting[:value]
			setting.save
		end 
	end
}
