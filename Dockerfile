FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

RUN mkdir /redmine
WORKDIR /redmine

COPY config/database.yml config/
COPY Gemfile Gemfile.lock docker-entrypoint.sh ./

RUN bundle install --binstubs
COPY . .

RUN bundle exec rake generate_secret_token && chmod +x docker-entrypoint.sh

ENTRYPOINT [ "/redmine/docker-entrypoint.sh" ]
